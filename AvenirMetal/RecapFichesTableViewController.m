//
//  RecapFichesTableViewController.m
//  AvenirMetal
//
//  Created by jb fuss on 07/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "RecapFichesTableViewController.h"
#import "FicheTableViewCell.h"
#import "ficheObject.h"
#import "Fiche.h"
#import "LastFichesViewController.h"
#import "AppDelegate.h"
#import "Data.h"


@interface RecapFichesTableViewController ()
    @property (strong,nonatomic) LastFichesViewController *lfvc;
    @property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
    @property (nonatomic,assign) UIActivityIndicatorView *spinner;
@end

@implementation RecapFichesTableViewController
@synthesize fiches;

- (void)viewDidLoad
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    [super viewDidLoad];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [fiches count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    FicheTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ficheObject *fiche = [fiches objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [dateFormatter setLocale:frLocale];
    [dateFormatter setDateFormat:@"HH:mm"];
    
    
    NSString *startMorning = [dateFormatter stringFromDate:fiche.matin_debut];
    NSString *endMorning = [dateFormatter stringFromDate:fiche.matin_fin];
    NSString *startAfternoon = [dateFormatter stringFromDate:fiche.aprem_debut];
    NSString *endAfternoon = [dateFormatter stringFromDate:fiche.aprem_fin];
    
    cell.nameEmployeeLabel.text = fiche.name_employee_output;
    
    if(fiche.matin_debut !=nil || fiche.matin_fin !=nil){
        
        cell.morningHoursLabel.text = [NSString stringWithFormat: @"%@ - %@",startMorning,endMorning];
    }
    
    if(fiche.aprem_debut !=nil || fiche.aprem_fin !=nil){
        
        cell.afternoonHoursLabel.text = [NSString stringWithFormat: @"%@ - %@",startAfternoon,endAfternoon];
        
    }
    
    
    if([fiche.granddeplacement boolValue]){
        cell.deplacementImage.hidden = NO;
    }
    else{
        cell.deplacementImage.hidden = YES;
    }
    
    if([fiche.repas boolValue]){
        cell.repasImage.hidden = NO;
    }
    else{
        cell.repasImage.hidden = YES;
    }
    
    if([fiche.trajet boolValue]){
        cell.trajet.hidden = NO;
    }
    else{
        cell.trajet.hidden = YES;
    }
    
    return cell;
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *nomsegue = @"endAddFiche";
    if([segue.identifier isEqualToString:nomsegue]){
        
        NSMutableDictionary *fichesMDforJSON = [self setMDforSendToAPI];
        
        [self startSpinner];
        
        [[WebService sharedInstance] addFiche:(id)self withJsonDictionnary:fichesMDforJSON];
        
        self.lfvc = [segue destinationViewController];
    }
    
}

- (void) wsPostAddFiche:(NSDictionary *)responseAdd{
    
    [self.spinner stopAnimating];
    
    if([responseAdd[@"success"] isEqualToString:@"true"]){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Votre fiche a bien été envoyée"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        // Report notif
        
        NSCalendar* cal = [NSCalendar currentCalendar];
        NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
        
        NSInteger nowWeekDay =[comp weekday];
        
        UIApplication *app = [UIApplication sharedApplication];
        NSArray *eventArray = [app scheduledLocalNotifications];
        for (int i=0; i<[eventArray count]; i++)
        {
            UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
            NSDictionary *userInfoCurrent = oneEvent.userInfo;
            NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"msg"]];
            
            comp = [cal components:NSCalendarUnitWeekday fromDate:oneEvent.fireDate];
            
            NSInteger notifWeekDay =[comp weekday];
            
            if ([uid isEqualToString:@"ficheNotSend"] && (notifWeekDay == nowWeekDay) )
            {
                
                [app cancelLocalNotification:oneEvent];
               
                int daysToAdd = 7;
                
                // set up date components
                NSDateComponents *components = [[NSDateComponents alloc] init];
                [components setDay:daysToAdd];
                
                // create a calendar
                NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                
                NSDate *nextWeek = [gregorian dateByAddingComponents:components toDate:oneEvent.fireDate options:0];
                
                NSDictionary *userDict = [NSDictionary dictionaryWithObject:@"ficheNotSend" forKey:@"msg"];
                
                UILocalNotification* newNotif = [[UILocalNotification alloc] init];
                newNotif.fireDate = nextWeek;
                newNotif.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
                newNotif.userInfo = userDict;
                newNotif.soundName = UILocalNotificationDefaultSoundName;
                newNotif.timeZone = [NSTimeZone defaultTimeZone];
                newNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                newNotif.repeatInterval = NSCalendarUnitWeekday;
                [[UIApplication sharedApplication] scheduleLocalNotification:newNotif];
                
            }
        }
        
        [self.lfvc configureView];
        
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Une erreur est arrivée"
                                                            message:@"Veuillez ressayer"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }
    
}

- (void) wsConnexionFailed{
    [self.spinner stopAnimating];
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"msg"]];
        if ([uid isEqualToString:@"failedSendFiche"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
        }
    }
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDate *now = [NSDate date];
    
    NSDateComponents *componentsForFireDate = [calendar components:(NSCalendarUnitYear | NSCalendarUnitWeekday |  NSCalendarUnitHour | NSCalendarUnitMinute| NSCalendarUnitSecond | NSCalendarUnitWeekday) fromDate: now];
    
    NSDictionary *userDict = [NSDictionary dictionaryWithObject:@"failedSendFiche" forKey:@"msg"];
    
    [componentsForFireDate setHour:20];
    [componentsForFireDate setMinute:0];
    [componentsForFireDate setSecond:0];
    
    //Notification Lundi
    
    UILocalNotification* notif = [[UILocalNotification alloc] init];
    notif.fireDate = [calendar dateFromComponents:componentsForFireDate];
    notif.alertBody = @"Vous devez envoyez une ou plusieurs fiches qui n'ont pas été envoyées";
    notif.soundName = UILocalNotificationDefaultSoundName;
    notif.userInfo = userDict;
    notif.timeZone = [NSTimeZone defaultTimeZone];
    notif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    notif.repeatInterval = NSCalendarUnitDay;
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    

    for(int i = 0; i<[fiches count]; i++){
        ficheObject *ficheToSave = [fiches objectForKey:[NSString stringWithFormat:@"%ld", (long)i]];

        Fiche *fiche = [NSEntityDescription insertNewObjectForEntityForName:@"Fiche" inManagedObjectContext:self.managedObjectContext];

        fiche.aprem_debut = ficheToSave.aprem_debut;
        fiche.aprem_fin = ficheToSave.aprem_fin;
        fiche.chantiers_has_chefs_chantiers_id = ficheToSave.chantiers_has_chefs_chantiers_id;
        fiche.chantiers_has_chefs_chefs_id = ficheToSave.chantiers_has_chefs_chefs_id;
        fiche.commentaire = ficheToSave.commentaire;
        fiche.employe_id = ficheToSave.employe_id;
        fiche.granddeplacement = ficheToSave.granddeplacement;
        fiche.jour = ficheToSave.jour;
        fiche.matin_debut = ficheToSave.matin_debut;
        fiche.matin_fin = ficheToSave.matin_fin;
        fiche.name_employee_output = ficheToSave.name_employee_output;
        fiche.repas = ficheToSave.repas;
        fiche.trajet = ficheToSave.trajet;
    
    }
    
    NSError *error;
    //On sauvegarde le context
    if (![self.managedObjectContext save:&error]) {
        
        
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion"
                                                        message:@"La fiche a été enregistrée - Elle sera renvoyée plus tard"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    
    [alertView show];
}

-(NSMutableDictionary*) setMDforSendToAPI
{
    
    NSMutableDictionary *fichesMDforJSON = [[NSMutableDictionary alloc] init];
   
    int i = 0;
    
    NSArray *keys = [fiches allKeys];
    
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending: NO];
    keys =  [keys sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];

    for (id aKey in keys) {
       
       ficheObject *fiche = [fiches objectForKey:aKey];
        
        NSDateFormatter * dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *jourString =  fiche.jour;
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *startMorning=@"";
        if(fiche.matin_debut!=nil){
           startMorning  = [dateFormatter stringFromDate:fiche.matin_debut];
        }
      
        NSString *endMorning=@"";
        if(fiche.matin_fin!=nil){
            endMorning = [dateFormatter stringFromDate:fiche.matin_fin];
        }
        
        NSString *startAfternoon=@"";
        if(fiche.aprem_debut!=nil){
            startAfternoon = [dateFormatter stringFromDate:fiche.aprem_debut];
        }
        
        NSString *endAfternoon=@"";
        if(fiche.aprem_fin!=nil){
            endAfternoon = [dateFormatter stringFromDate:fiche.aprem_fin];
        }
        
        
        if(fiche.granddeplacement==NULL){
            fiche.granddeplacement = [NSNumber numberWithInt:0];
        }
        
        if(fiche.repas==NULL){
            fiche.repas = [NSNumber numberWithInt:0];
        }
        
        if(fiche.trajet==NULL){
            fiche.trajet = [NSNumber numberWithInt:0];
        }
        
    
        NSDictionary *ficheDict =
        @{
          @"jour" : jourString,
          @"chantiers_has_chefs_chantiers_id" : fiche.chantiers_has_chefs_chantiers_id,
          @"chantiers_has_chefs_chefs_id" : fiche.chantiers_has_chefs_chefs_id,
          @"employe_id" : fiche.employe_id,
          @"matin_debut" : startMorning,
          @"matin_fin" : endMorning,
          @"aprem_debut": startAfternoon,
          @"aprem_fin": endAfternoon,
          @"trajet":fiche.trajet,
          @"repas":fiche.repas,
          @"granddeplacement":fiche.granddeplacement,
          };


        fichesMDforJSON[[NSString stringWithFormat: @"%d", i]]=ficheDict;
        
        i++;
    }



    return fichesMDforJSON;
}

-(void)startSpinner{
    UIActivityIndicatorView *spinnerObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = spinnerObject;
    self.spinner.center = CGPointMake(160, 240);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

@end
