//
//  profilListeAdministrationViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 11/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"


@interface profilListeAdministrationViewController : UITableViewController<WebServiceDelegate,UISearchBarDelegate, UISearchDisplayDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
