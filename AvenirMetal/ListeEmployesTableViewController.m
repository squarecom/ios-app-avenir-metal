//
//  ListeEmployesTableViewController.m
//  AvenirMetal
//
//  Created by jb fuss on 20/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "ListeEmployesTableViewController.h"
#import "Data.h"
#import "ListeInterimairesViewController.h"
#import "ficheObject.h"
#import "coreDataRequests.h"

@interface ListeEmployesTableViewController ()
    @property (nonatomic, assign) NSInteger numberEmployes;
    @property (strong) NSArray* employes;
    @property (strong) NSMutableSet* employesSelect;
    @property (strong, nonatomic) NSArray *searchResults;
    @property (nonatomic, assign) BOOL isSearch;
    @property (strong, nonatomic) NSMutableArray *mutableEmployes;
    @property (nonatomic,assign) UIActivityIndicatorView *spinner;
    @property (nonatomic,strong) coreDataRequests *requests;

@end

@implementation ListeEmployesTableViewController
@synthesize tableview;
@synthesize searchBar;
@synthesize fiches;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.employesSelect  = [NSMutableSet set];
    
    self.requests = [[coreDataRequests alloc] init];
    
    searchBar.delegate = (id)self;
    searchBar.showsCancelButton = YES;
    [searchBar setShowsCancelButton:YES animated:YES];
    [self startSpinner];
    [[WebService sharedInstance] getListeEmployes:(id)self];
    
}

- (void) wsPostGetEmployesCallBack:(NSDictionary *)employesDict {
    
    [self.spinner stopAnimating];
    
    self.employes = (NSArray *)employesDict;
    
    [self.requests updateEmployesNotInterim:self.employes];
    
    self.mutableEmployes = [NSMutableArray arrayWithArray:self.employes];
    
    if(fiches!=nil){
        
        [self initializeEmployesSelect];
        
    }
    [self sortEmployes];
}

- (void) wsPostGetEmployesFailed{
    
    [self.spinner stopAnimating];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Assurez vous d'avoir une connexion internet"
                                                        message:@"Les données peuvent ne pas être à jour"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
    
    self.employes = [self.requests getEmployesNotInterim];

    self.mutableEmployes = [NSMutableArray arrayWithArray:self.employes];
    if(fiches!=nil){
        
        [self initializeEmployesSelect];
        
    }
    [self sortEmployes];
    
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    if (self.isSearch) {
        return [self.searchResults count];
        
    } else {
        return [self.employes count];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"employeCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"employeCell"];
    }
    
    NSDictionary *employeDetail;
    if (self.isSearch) {
        employeDetail  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        employeDetail = [self.employes objectAtIndex:indexPath.row];
    }
    cell.textLabel.text = [NSString stringWithFormat: @"%@ %@",employeDetail[@"Employe"][@"nom"],employeDetail[@"Employe"][@"prenom"]];
    
    if ([self.employesSelect containsObject:employeDetail])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    NSDictionary *employeDetail;
    if (self.isSearch) {
        employeDetail  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        employeDetail = [self.employes objectAtIndex:indexPath.row];
    }
    
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType != UITableViewCellAccessoryCheckmark){
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [self.employesSelect addObject:employeDetail];
       
    }else{
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [self.employesSelect removeObject:employeDetail];
      
    }
    
    [self sortEmployes];
}


-(void)setNumberEmployesToSelect:(NSInteger)numberEmployes{
    self.numberEmployes = numberEmployes;
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        self.isSearch = FALSE;
        
    }
    else
    {
        self.isSearch = TRUE;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Employe.nom contains[cd] %@ || SELF.Employe.prenom contains[cd] %@  ",
                                        text,text];
        
        self.searchResults = [self.mutableEmployes filteredArrayUsingPredicate:resultPredicate];

    }
    [self.tableView reloadData];
}

-(void) searchBarSearchButtonClicked:(UISearchBar*) searchBarObject {
    //do some thing
    [self.view endEditing:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBarObject
{
    searchBar.text=@"";
    self.isSearch = FALSE;
    [self.tableView reloadData];
    [self.view endEditing:YES];
}

-(void) sortEmployes{
   
    NSMutableArray *employeMA = [[NSMutableArray alloc] init];
    /* ON AJOUTE D'ABORD LES EMPLOYES SELECTIONNÉS */
    for(NSDictionary *employeDetail in self.employes){
        
        if ([self.employesSelect containsObject:employeDetail])
        {
            [employeMA addObject:employeDetail];
        }

    }
    
    for(NSDictionary *employeDetail in self.employes){
        
        if (![self.employesSelect containsObject:employeDetail])
        {
            [employeMA addObject:employeDetail];
        }
    }
    
    self.employes = [NSArray arrayWithArray:employeMA];
    [self.tableView reloadData];
    
}


-(void) initializeEmployesSelect{
    
    for(ficheObject *fiche in fiches){
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"Employe.id == %@",
                                        [fiche.employe_id stringValue]];
        
        NSArray *employeArray = [self.mutableEmployes filteredArrayUsingPredicate:resultPredicate];
        NSDictionary *employeDetail;
        
        if([employeArray count]==1){
            
            employeDetail = [employeArray objectAtIndex:0];
            [self.employesSelect addObject:employeDetail];
        }
    }
    

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *nomsegue = @"selectioninterimaires";
    if([segue.identifier isEqualToString:nomsegue]){
        if([self.employesSelect count]<=self.numberEmployes){
            ListeInterimairesViewController* litvc;
            litvc = [segue destinationViewController];
            [litvc setNumberEmployes:self.numberEmployes];
           [litvc setEmployesSelect:self.employesSelect];
            [litvc setFiches:fiches];
        }
        
    }
}

 - (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
     NSString *nomsegue = @"selectioninterimaires";
     if([identifier isEqualToString:nomsegue]){
         
         if([self.employesSelect count]>self.numberEmployes){
             
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat: @"Vous devez selectionner au maximum %ld employé",(long)self.numberEmployes] message:[NSString stringWithFormat: @"%ld selectionnés",(long)[self.employesSelect count]]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [alertView show];
             return NO;
         }else{
             return YES;
         }
         
     }

     return YES;
 }

-(void)startSpinner{
    UIActivityIndicatorView *spinnerObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = spinnerObject;
    self.spinner.center = CGPointMake(160, 240);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}


@end
