//
//  main.m
//  AvenirMetal
//
//  Created by jb fuss on 17/04/14.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
