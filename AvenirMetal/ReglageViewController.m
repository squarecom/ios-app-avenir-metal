//
//  ReglageViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 10/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "ReglageViewController.h"
#import "Data.h"

@interface ReglageViewController ()
@property (strong, nonatomic) IBOutlet UISwitch *SwitchNotif;
@property (strong, nonatomic) IBOutlet UIDatePicker *datepickernotif;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveregbutton;


@end

@implementation ReglageViewController
@synthesize switchAlarm;



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.datepickernotif.datePickerMode = UIDatePickerModeTime;
    
    // Fetch a default
    NSString *stringDateNotif = [[NSUserDefaults standardUserDefaults] stringForKey:@"dateNotif"];
    
    NSString *valueSwitch= [[NSUserDefaults standardUserDefaults] stringForKey:@"valueSwitch"];
    
    
    // Save a default
    //[[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"DeleteBackup"];
    
    if(stringDateNotif == nil){
        
       [self.datepickernotif setDate:[NSDate date]];
        
    }else{
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *dateNotif = [dateFormatter dateFromString:stringDateNotif];
        
        [self.datepickernotif setDate:dateNotif];
        
    }
    
    if(valueSwitch !=nil){
        if([valueSwitch isEqualToString:@"on"]){
            
            [switchAlarm setOn:YES animated:YES];
            
        }else{
            [switchAlarm setOn:NO animated:YES];
        }
    }
    
    self.datepickernotif.timeZone = [NSTimeZone localTimeZone];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [self.datepickernotif setLocale:frLocale];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)saveReglages:(id)sender {
   
    [self changeSettings];

}

- (void)changeSettings {
    
    [self deleteNotifications];
    if(switchAlarm.on){
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDate *pickerDate = [self.datepickernotif date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *dateNotif = [dateFormatter stringFromDate:pickerDate];
        
        // Save a default
        [[NSUserDefaults standardUserDefaults] setObject:dateNotif forKey:@"dateNotif"];
        
        NSDateComponents *componentsForFireDate = [calendar components:(NSCalendarUnitYear| NSCalendarUnitWeekday |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday) fromDate: pickerDate];
        
        NSDictionary *userDict = [NSDictionary dictionaryWithObject:@"ficheNotSend" forKey:@"msg"];
        
        [componentsForFireDate setWeekday:2] ; //Lundi
        [componentsForFireDate setSecond:0];
        
        //Notification Lundi
        
        UILocalNotification* notifMonday = [[UILocalNotification alloc] init];
        notifMonday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifMonday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifMonday.soundName = UILocalNotificationDefaultSoundName;
        notifMonday.userInfo = userDict;
        notifMonday.timeZone = [NSTimeZone defaultTimeZone];
        notifMonday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifMonday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifMonday];
        
        
        [componentsForFireDate setWeekday:3] ; //Mardi
        [componentsForFireDate setSecond:0];
        
        //Notification Mardi
        
        UILocalNotification* notifTuesday = [[UILocalNotification alloc] init];
        notifTuesday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifTuesday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifTuesday.userInfo = userDict;
        notifTuesday.soundName = UILocalNotificationDefaultSoundName;
        notifTuesday.timeZone = [NSTimeZone defaultTimeZone];
        notifTuesday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifTuesday.repeatInterval =  NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifTuesday];
        
        [componentsForFireDate setWeekday:4] ; //Mercredi
        [componentsForFireDate setSecond:0];
        
        //Notification Mercredi
        
        UILocalNotification* notifWednesday = [[UILocalNotification alloc] init];
        notifWednesday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifWednesday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifWednesday.userInfo = userDict;
        notifWednesday.soundName = UILocalNotificationDefaultSoundName;
        notifWednesday.timeZone = [NSTimeZone defaultTimeZone];
        notifWednesday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifWednesday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifWednesday];
        
        [componentsForFireDate setWeekday:5] ; //Jeudi
        [componentsForFireDate setSecond:0];
        
        //Notification Jeudi
        
        UILocalNotification* notifThursday = [[UILocalNotification alloc] init];
        notifThursday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifThursday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifThursday.userInfo = userDict;
        notifThursday.soundName = UILocalNotificationDefaultSoundName;
        notifThursday.timeZone = [NSTimeZone defaultTimeZone];
        notifThursday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifThursday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifThursday];
        
        [componentsForFireDate setWeekday:6] ; //Vendredi
        [componentsForFireDate setSecond:0];
        
        //Notification Vendredi
        
        UILocalNotification* notifFriday = [[UILocalNotification alloc] init];
        notifFriday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifFriday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifFriday.userInfo = userDict;
        notifFriday.soundName = UILocalNotificationDefaultSoundName;
        notifFriday.timeZone = [NSTimeZone defaultTimeZone];
        notifFriday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifFriday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifFriday];
        
        //
        // Request to reload table view data
        // [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
        
        // Dismiss the view controller
        //[self dismissViewControllerAnimated:YES completion:nil];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Réglages mis à jour"
                                                            message:@"L'heure de la notification a bien été mise à jour"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];

        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alerte désactivée"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
    
}

- (IBAction)switchChanged:(id)sender {
    
    if(switchAlarm.on){
        [[NSUserDefaults standardUserDefaults] setObject:@"on" forKey:@"valueSwitch"];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"off" forKey:@"valueSwitch"];
    }
    [self changeSettings];
    
}

-(void) deleteNotifications {
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"msg"]];
        if ([uid isEqualToString:@"ficheNotSend"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
        }
    }
    
}

@end
