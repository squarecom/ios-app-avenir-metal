//
//  profilListeAdministrationViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 11/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "profilListeAdministrationViewController.h"
#import "profilDetailsAdministrationViewController.h"
#import "contactAdminTableViewCell.h"
#import "coreDataRequests.h"

@interface profilListeAdministrationViewController ()

    @property (strong) NSArray* administration;
    @property (strong, nonatomic) profilDetailsAdministrationViewController *pdavc;
    @property (nonatomic,assign) UIActivityIndicatorView *spinner;
    @property (strong, nonatomic) NSArray *searchResults;
    @property (nonatomic, assign) BOOL isSearch;
    @property (strong, nonatomic) NSMutableArray *mutableContacts;
    @property (strong, nonatomic) coreDataRequests *requests;

@end

@implementation profilListeAdministrationViewController
@synthesize searchBar;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.requests = [[coreDataRequests alloc]init];
    
    searchBar.delegate = (id)self;
    searchBar.showsCancelButton = YES;
    [searchBar setShowsCancelButton:YES animated:YES];
    
    [self startSpinner];
    
    [[WebService sharedInstance] getListeAdministration:(id)self];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) wsPostGetListeAdministrationCallBack:(NSDictionary *)administrationDict {
    [self.spinner stopAnimating];
    
    self.administration = (NSArray *)administrationDict;
    
    [self.requests updateContacts:self.administration];
    
     self.mutableContacts = [NSMutableArray arrayWithArray:self.administration];
    
    [self.tableView reloadData];

}

- (void) wsPostGetContactsFailed{
    
    [self.spinner stopAnimating];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Assurez vous d'avoir une connexion internet"
                                                        message:@"Les données peuvent ne pas être à jour"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
    
    self.administration = [self.requests getContacts];
    
    self.mutableContacts = [NSMutableArray arrayWithArray:self.administration];
    
    [self.tableView reloadData];
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.isSearch) {
        return [self.searchResults count];
        
    } else {
         return [self.administration count];
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"administrationcell";
    contactAdminTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    NSDictionary *administrationDetail;
    if (self.isSearch) {
        administrationDetail  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        administrationDetail = [self.administration objectAtIndex:indexPath.row];
    }
    
    cell.cellName.text = [NSString stringWithFormat: @"%@ %@",administrationDetail[@"Contact"][@"nom"],administrationDetail[@"Contact"][@"prenom"]];
    cell.cellStatut.text = [NSString stringWithFormat: @"%@",administrationDetail[@"Contact"][@"statut"]];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *administrationDetail;
    if (self.isSearch) {
        administrationDetail  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        administrationDetail = [self.administration objectAtIndex:indexPath.row];
    }
    
    self.pdavc.detailsAdministration = administrationDetail[@"Contact"];
    
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"detailsAdministrationSegue"]){
        
        //On recupere le destination view controller
        self.pdavc = [segue destinationViewController];
        
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        self.isSearch = FALSE;
        
    }
    else
    {
        self.isSearch = TRUE;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Contact.nom contains[cd] %@ || SELF.Contact.prenom contains[cd] %@ || SELF.Contact.statut contains[cd] %@ ",
                                        text,text,text];
        
        self.searchResults = [self.mutableContacts filteredArrayUsingPredicate:resultPredicate];
        
    }
    [self.tableView reloadData];
}

-(void) searchBarSearchButtonClicked:(UISearchBar*) searchBarObject {
    //do some thing
    [self.view endEditing:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBarObject
{
    searchBar.text=@"";
    self.isSearch = FALSE;
    [self.tableView reloadData];
    [self.view endEditing:YES];
}


-(void)startSpinner{
    UIActivityIndicatorView *spinnerObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = spinnerObject;
    self.spinner.center = CGPointMake(160, 240);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}


@end
