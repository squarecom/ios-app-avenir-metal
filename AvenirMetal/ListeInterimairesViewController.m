//
//  ListeInterimairesViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 21/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "ListeInterimairesViewController.h"
#import "ListeEmployeesSelectedTableViewController.h"
#import "ficheObject.h"
#import "coreDataRequests.h"

@interface ListeInterimairesViewController ()

    @property (strong) NSArray* interimaires;
    @property (strong) NSMutableDictionary* employesNonInterimaireSelected;
    @property (strong) NSMutableDictionary* employesInterimaireSelected;
    @property (strong) NSMutableDictionary* allEmployesSelected;
    @property (strong, nonatomic) NSArray *searchResults;
    @property (nonatomic, assign) BOOL isSearch;
    @property (strong, nonatomic) NSMutableArray *mutableInterimaires;
    @property (nonatomic,assign) UIActivityIndicatorView *spinner;
    @property (strong, nonatomic) coreDataRequests *requests;

@end

@implementation ListeInterimairesViewController
@synthesize searchBar;
@synthesize numberEmployes;
@synthesize employesSelect;
@synthesize fiches;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.employesInterimaireSelected = [[NSMutableDictionary alloc] init];
    
    searchBar.delegate = (id)self;
    searchBar.showsCancelButton = YES;
    [searchBar setShowsCancelButton:YES animated:YES];
    
    self.requests = [[coreDataRequests alloc]init];
    
    [self startSpinner];
    [[WebService sharedInstance] getListeInterimaires:(id)self];
   
}
- (void) wsPostGetInterimairesCallBack:(NSDictionary *)interimairesDict {
    [self.spinner stopAnimating];
    self.interimaires = (NSArray *)interimairesDict;
    self.mutableInterimaires = [NSMutableArray arrayWithArray:self.interimaires];
    [self.requests updateInterims:self.interimaires];
    
    if(fiches!=nil){
        
        [self initializeEmployesSelect];
        
    }
    
    [self sortInterimaires];
}


- (void) wsPostGetInterimsFailed{
    
    [self.spinner stopAnimating];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Assurez vous d'avoir une connexion internet"
                                                        message:@"Les données peuvent ne pas être à jour"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
    
    self.interimaires = [self.requests getInterims];
    
    self.mutableInterimaires = [NSMutableArray arrayWithArray:self.interimaires];
    
    if(fiches!=nil){
        
        [self initializeEmployesSelect];
        
    }
    
    [self sortInterimaires];
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isSearch) {
        return [self.searchResults count];
        
    } else {
        return [self.interimaires count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"interimaireCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"interimaireCell"];
    }
    
    NSDictionary *interimairesDetail;
    
    if (self.isSearch) {
        interimairesDetail  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        interimairesDetail = [self.interimaires objectAtIndex:indexPath.row];
    }
    
    cell.textLabel.text = [NSString stringWithFormat: @"%@ %@",interimairesDetail[@"Employe"][@"nom"],interimairesDetail[@"Employe"][@"prenom"]];
    
    if ([self.employesSelect containsObject:interimairesDetail])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    NSDictionary *interimairesDetail;
    
    if (self.isSearch) {
        interimairesDetail  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        interimairesDetail = [self.interimaires objectAtIndex:indexPath.row];
    }
    
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType != UITableViewCellAccessoryCheckmark){
        
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        [self.employesSelect addObject:interimairesDetail];
        
    }else{
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        [self.employesSelect removeObject:interimairesDetail];
    }
    
    [self sortInterimaires];
}

-(void) initializeEmployesSelect{
    
    for(ficheObject *fiche in fiches){
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Employe.id == %@",
                                        [fiche.employe_id stringValue]];
        
        NSArray *employeArray = [self.mutableInterimaires filteredArrayUsingPredicate:resultPredicate];
        NSDictionary *employeDetail;
        
        if([employeArray count]==1){
            employeDetail = [employeArray objectAtIndex:0];
            [self.employesSelect addObject:employeDetail];
        }
    }
    
}

-(void) sortInterimaires{
    
   NSMutableArray *interimaireMS = [[NSMutableArray alloc] init];
    
    /* ON AJOUTE D'ABORD LES EMPLOYES SELECTIONNÉS */
    for(NSDictionary *employeDetail in self.interimaires){
        
        if ([self.employesSelect containsObject:employeDetail])
        {
            [interimaireMS addObject:employeDetail];
        }
        
    }
    
    for(NSDictionary *employeDetail in self.interimaires){
        
        if (![self.employesSelect containsObject:employeDetail])
        {
            [interimaireMS addObject:employeDetail];
        }
    }
    
    self.interimaires = [NSArray arrayWithArray:interimaireMS];
    
    [self.tableView reloadData];
    
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *nomsegue = @"endSelectEmployes";
    if([segue.identifier isEqualToString:nomsegue]){

        if([employesSelect count]==numberEmployes){
            
            ListeEmployeesSelectedTableViewController* lestvc;
            lestvc = [segue destinationViewController];
            [lestvc setEmployeesSelected:[employesSelect allObjects]];
            [lestvc setLastFiches:fiches];
            
    }
        
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {

    NSString *nomsegue = @"endSelectEmployes";
    if([identifier isEqualToString:nomsegue]){
        if([employesSelect count]>numberEmployes){
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat: @"Vous devez selectionner au maximum %ld employé",(long)numberEmployes] message:[NSString stringWithFormat: @"%ld selectionnés",(long)[employesSelect count]]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
            return NO;
        }else{
            if([employesSelect count]<numberEmployes){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat: @"Vous devez selectionner au minimum %ld employé",(long)numberEmployes] message:[NSString stringWithFormat: @"%ld selectionnés",(long)[employesSelect count]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
                return NO;
            }else{
                return YES;
            }
        }
        
    }
    
    return YES;
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        self.isSearch = FALSE;
        
    }
    else
    {
        self.isSearch = TRUE;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Employe.nom contains[cd] %@ || SELF.Employe.prenom contains[cd] %@  ",
                                        text,text];
        
        self.searchResults = [self.mutableInterimaires filteredArrayUsingPredicate:resultPredicate];
        
    }
    [self.tableView reloadData];
}

-(void) searchBarSearchButtonClicked:(UISearchBar*) searchBarObject {
    //do some thing
    [self.view endEditing:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBarObject
{
    searchBar.text=@"";
    self.isSearch = FALSE;
    [self.tableView reloadData];
    [self.view endEditing:YES];
}

-(void)startSpinner{
    UIActivityIndicatorView *spinnerObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = spinnerObject;
    self.spinner.center = CGPointMake(160, 240);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

@end
