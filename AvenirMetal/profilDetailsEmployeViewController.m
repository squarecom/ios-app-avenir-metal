//
//  profilDetailsEmployeViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 09/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "profilDetailsEmployeViewController.h"

@interface profilDetailsEmployeViewController ()
@property (strong, nonatomic) IBOutlet UITableViewCell *namecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *telcell;
@property (strong, nonatomic) IBOutlet UITableViewCell *adrcell;
@property (strong, nonatomic) IBOutlet UITableViewCell *villecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *comcell;
@property (strong, nonatomic) IBOutlet UIButton *telbutton;
@property (strong, nonatomic) NSString *tel;


@end

@implementation profilDetailsEmployeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    NSString *nomprenom = [NSString stringWithFormat:@"%@ %@",self.detailsEmploye[@"nom"],self.detailsEmploye[@"prenom"]];
                           
    self.namecell.textLabel.text= nomprenom;
    //self.telbutton.textLabel = self.detailsEmploye[@"tel"];
    NSString *protoctel = @"telprompt://";
    
    
    self.tel = [NSString stringWithFormat:@"%@%@", protoctel, self.detailsEmploye[@"tel"]];
    
    
    [self.telbutton setTitle:self.detailsEmploye[@"tel"] forState:
     (UIControlStateNormal)];
    
    
    self.adrcell.textLabel.text = self.detailsEmploye[@"adresse"];
    self.villecell.textLabel.text = self.detailsEmploye[@"ville"];
    self.comcell.textLabel.text = self.detailsEmploye[@"commentaire"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callNumber:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.tel]];
    
}




/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
