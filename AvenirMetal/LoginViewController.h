//
//  LoginViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 27/04/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface LoginViewController : UIViewController<WebServiceDelegate,UIScrollViewDelegate, UITextFieldDelegate>

//- (NSDictionary *) checkLogin:( NSString *)login andPassword:( NSString *)password;
- (IBAction)connecTapped:(id)sender;
- (IBAction)settingsTapped:(id)sender;

@end
