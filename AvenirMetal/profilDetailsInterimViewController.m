//
//  profilDetailsInterimViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 09/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "profilDetailsInterimViewController.h"

@interface profilDetailsInterimViewController ()
@property (strong, nonatomic) IBOutlet UITableViewCell *namecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *telcell;
@property (strong, nonatomic) IBOutlet UITableViewCell *adressecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *villecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *nameagencecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *telagencecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *comcell;
@property (strong, nonatomic) IBOutlet UIButton *telbutton;
@property (strong, nonatomic) NSString *tel;
@property (strong, nonatomic) IBOutlet UITableViewCell *villeagencecell;
@property (strong, nonatomic) IBOutlet UIButton *telagencebutton;
@property (strong, nonatomic) NSString *telagence;



@end

@implementation profilDetailsInterimViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSString *nomprenom = [NSString stringWithFormat:@"%@ %@",self.detailsInterim[@"Employe"][@"nom"],self.detailsInterim[@"Employe"][@"prenom"]];
    
    self.namecell.textLabel.text= nomprenom;
    //self.telcell.textLabel.text = self.detailsInterim[@"tel"];
    NSString *protoctel = @"telprompt://";
    
    
    self.tel = [NSString stringWithFormat:@"%@%@", protoctel, self.detailsInterim[@"Employe"][@"tel"]];
    
    [self.telbutton setTitle:self.detailsInterim[@"Employe"][@"tel"] forState:
     (UIControlStateNormal)];
    
    self.adressecell.textLabel.text = self.detailsInterim[@"Employe"][@"adresse"];
    self.villecell.textLabel.text = self.detailsInterim[@"Employe"][@"ville"];
    
    self.telagence = [NSString stringWithFormat:@"%@%@", protoctel, self.detailsInterim[@"Agence"][@"tel"]];

    
    [self.telagencebutton setTitle:self.detailsInterim[@"Agence"][@"tel"] forState:
     (UIControlStateNormal)];
    
    self.nameagencecell.textLabel.text = self.detailsInterim[@"Agence"][@"nom"];
    
    self.villeagencecell.textLabel.text = self.detailsInterim[@"Agence"][@"ville"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callNumber:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.tel]];
}

- (IBAction)callNumberAgence:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.telagence]];

}

#pragma mark - Table view data source



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */


@end
