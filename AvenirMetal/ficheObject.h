//
//  ficheObject.h
//  AvenirMetal
//
//  Created by jb fuss on 01/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ficheObject : NSObject

    @property (nonatomic, retain) NSDate * aprem_debut;
    @property (nonatomic, retain) NSDate * aprem_fin;
    @property (nonatomic, retain) NSNumber * chantiers_has_chefs_chantiers_id;
    @property (nonatomic, retain) NSNumber * chantiers_has_chefs_chefs_id;
    @property (nonatomic, retain) NSString * commentaire;
    @property (nonatomic, retain) NSNumber * employe_id;
    @property (nonatomic, retain) NSNumber * granddeplacement;
    @property (nonatomic, retain) NSString * jour;
    @property (nonatomic, retain) NSDate * matin_debut;
    @property (nonatomic, retain) NSDate * matin_fin;
    @property (nonatomic, retain) NSString * name_employee_output;
    @property (nonatomic, retain) NSNumber * repas;
    @property (nonatomic, retain) NSNumber * trajet;

@end
