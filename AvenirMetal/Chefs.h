//
//  Chefs.h
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Assignations_chantier, Employes, LastFiches;

@interface Chefs : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *assignations;
@property (nonatomic, retain) Employes *employe_id;
@property (nonatomic, retain) NSSet *fiches;
@end

@interface Chefs (CoreDataGeneratedAccessors)

- (void)addAssignationsObject:(Assignations_chantier *)value;
- (void)removeAssignationsObject:(Assignations_chantier *)value;
- (void)addAssignations:(NSSet *)values;
- (void)removeAssignations:(NSSet *)values;

- (void)addFichesObject:(LastFiches *)value;
- (void)removeFichesObject:(LastFiches *)value;
- (void)addFiches:(NSSet *)values;
- (void)removeFiches:(NSSet *)values;

@end
