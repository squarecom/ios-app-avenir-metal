//
//  selectHoursTableViewController.m
//  AvenirMetal
//
//  Created by jb fuss on 07/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "selectHoursTableViewController.h"
#import "ficheObject.h"
#import "selectTrajetsTableViewController.h"

@interface selectHoursTableViewController ()

/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@property (nonatomic, strong) ficheObject *currentFiche;
@property (nonatomic, strong) ficheObject *firstFiche;
@property (nonatomic,assign) NSInteger currentIndexFiches;
@property (nonatomic,strong)UIActionSheet *pickerAction;
@property (nonatomic, strong) UIGestureRecognizer *tapper; //Objet gestion évènement pour desactiver le clavier
@property (nonatomic, strong) NSString *labelSelected;
@property (nonatomic, strong) NSString *labelSelectedMessage;
@property (nonatomic, strong) NSDate *minMorning;
@property (nonatomic, strong) NSDate *maxMorning;
@property (nonatomic, strong) NSDate *minAfternoon;
@property (nonatomic, strong) NSDate *maxAfternoon;
@property (nonatomic, strong) NSDate *selectedStartMorning;
@property (nonatomic, strong) NSDate *selectedStartAfternoon;
@property (nonatomic, strong) NSDate *selectedEndMorning;
@property (nonatomic, strong) NSDate *selectedEndAfternoon;
@property (nonatomic, strong) UIAlertController * alert;

@end

@implementation selectHoursTableViewController

@synthesize nameEmployeeLabel;
@synthesize startMorningLabel;
@synthesize endMorningLabel;
@synthesize startAfternoonLabel;
@synthesize endAfternoonLabel;
@synthesize fiches;
@synthesize datePicker;
@synthesize toolBar;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.datePicker.hidden = true;
    self.toolBar.hidden = true;
    self.datePicker.datePickerMode = UIDatePickerModeTime;
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [self.datePicker setLocale:frLocale];
    self.datePicker.minuteInterval = 15;
    
    self.tapper = [[UITapGestureRecognizer alloc]
                   initWithTarget:self action:@selector(handleSingleTap:)];//Event simple touché
    self.tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:self.tapper];
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    [components setHour:00];
    self.minMorning = [calendar dateFromComponents:components];
    [components setHour:07];
    self.selectedStartMorning = [calendar dateFromComponents:components];
    [components setHour:13];
    self.maxMorning = [calendar dateFromComponents:components];
    [components setHour:12];
    self.selectedEndMorning = [calendar dateFromComponents:components];
    self.minAfternoon = [calendar dateFromComponents:components];
    [components setHour:14];
    self.selectedStartAfternoon = [calendar dateFromComponents:components];
    [components setHour:18];
    self.selectedEndAfternoon = [calendar dateFromComponents:components];
    [components setHour:23];
    self.maxAfternoon = [calendar dateFromComponents:components];
    
    
    
    [self configureView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) configureView {
    
    if(self.currentIndexFiches!=[fiches count]-1){
        self.navigationItem.rightBarButtonItem.title = @"Suivant";
    }else{
        self.navigationItem.rightBarButtonItem.title = @"Valider";
    }
    
    if(self.currentIndexFiches!=0){
        
        
        self.navigationItem.hidesBackButton = YES;
        
        UIBarButtonItem *prevButton = [[UIBarButtonItem alloc]
         initWithTitle:@"Précédent"
         style:UIBarButtonItemStylePlain
         target:self
         action:@selector(prevFiche)];
         self.navigationItem.leftBarButtonItem = prevButton;
        
    }else{
        
        self.navigationItem.hidesBackButton = NO;
        self.navigationItem.leftBarButtonItem = nil;
        
    }
    self.currentFiche = [fiches objectForKey:[NSString stringWithFormat:@"%ld", (long)self.currentIndexFiches]];
   
    if(self.firstFiche!=nil){
        
        if(self.currentFiche.matin_debut==nil && self.currentFiche.matin_fin==nil && self.currentFiche.aprem_debut==nil && self.currentFiche.aprem_fin==nil){
            
            self.currentFiche.matin_debut=self.firstFiche.matin_debut;
            self.currentFiche.matin_fin=self.firstFiche.matin_fin;
            self.currentFiche.aprem_debut=self.firstFiche.aprem_debut;
            self.currentFiche.aprem_fin=self.firstFiche.aprem_fin;
            
        }
        
    }
    nameEmployeeLabel.text = self.currentFiche.name_employee_output;
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [dateFormatter setLocale:frLocale];
    [dateFormatter setDateFormat:@"HH:mm"];
   
    
    if(self.currentFiche.matin_debut!=nil){
         NSString *dateString = [dateFormatter stringFromDate:self.currentFiche.matin_debut];
        startMorningLabel.text = [NSString stringWithFormat: @"%@",dateString];
        
    }else{
        startMorningLabel.text=@"Modifier";
    }
    
    if(self.currentFiche.matin_fin != nil){
         NSString *dateString = [dateFormatter stringFromDate:self.currentFiche.matin_fin];
        endMorningLabel.text = [NSString stringWithFormat: @"%@",dateString];
    }else{
        
        endMorningLabel.text=@"Modifier";
    }
    
    if(self.currentFiche.aprem_debut != nil){
         NSString *dateString = [dateFormatter stringFromDate:self.currentFiche.aprem_debut];
        startAfternoonLabel.text = [NSString stringWithFormat: @"%@",dateString];
    }else{
        startAfternoonLabel.text=@"Modifier";
    }
    
    if(self.currentFiche.aprem_fin != nil){
         NSString *dateString = [dateFormatter stringFromDate:self.currentFiche.aprem_fin];
         endAfternoonLabel.text = [NSString stringWithFormat: @"%@",dateString];
    }else{
        endAfternoonLabel.text=@"Modifier";
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * theCell = (UITableViewCell *)[tableView
                                                    cellForRowAtIndexPath:indexPath];
    
    CGPoint tableViewCenter = [tableView contentOffset];
    tableViewCenter.y += self.tableView.frame.size.height;
    
    [self.tableView setContentOffset:CGPointMake(0,theCell.center.y-100) animated:YES];
    
    if(indexPath.section==1 && indexPath.row==0){ //// Debut matin /////
        self.labelSelected = @"startMorning";
        self.labelSelectedMessage = @"début matin";
        [self showDatePicker];
        
    }
    if(indexPath.section==1 && indexPath.row==1){ //// Fin matin /////
        self.labelSelected = @"endMorning";
        self.labelSelectedMessage = @"fin matin";
        [self showDatePicker];
    }
    if(indexPath.section==1 && indexPath.row==2){ //// Debut aprem /////
        self.labelSelected = @"startAfternoon";
        self.labelSelectedMessage = @"début après midi";
        [self showDatePicker];
    }
    if(indexPath.section==1 && indexPath.row==3){ //// Fin aprem /////
        self.labelSelected = @"endAfternoon";
        self.labelSelectedMessage = @"fin après midi";
        [self showDatePicker];
        
    }
    

}

-(void) showDatePicker{
    
    self.datePicker.hidden = false;
    self.toolBar.hidden = false;
    [self configureDatePicker];
  

}

-(void) hideDatePicker{
    self.datePicker.hidden = true;
    self.toolBar.hidden = true;
}

-(void) configureDatePicker{

    if([self.labelSelected isEqualToString:@"startMorning"]){
        
        if(self.currentFiche.matin_debut!=nil){
            [self.datePicker setDate:self.currentFiche.matin_debut animated:YES] ; //selectionné la date (date de la tache)
        }
        else{
            [self.datePicker setDate:self.selectedStartMorning animated:YES];
        }
        
        [self.datePicker setMinimumDate:self.minMorning];
        [self.datePicker setMaximumDate:self.maxMorning];
        
    }
    if([self.labelSelected isEqualToString:@"endMorning"]){
        
        if(self.currentFiche.matin_fin!=nil){
            [self.datePicker setDate:self.currentFiche.matin_fin animated:YES]; //selectionné la date (date de la tache)
        }
        else{
            [self.datePicker setDate:self.selectedEndMorning animated:YES];
        }
        
        [self.datePicker setMinimumDate:self.minMorning];
        [self.datePicker setMaximumDate:self.maxMorning];
        
    }
    if([self.labelSelected isEqualToString:@"startAfternoon"]){
        
        if(self.currentFiche.aprem_debut!=nil){
            [self.datePicker setDate:self.currentFiche.aprem_debut animated:YES]; //selectionné la date (date de la tache)
        }
        else{
            [self.datePicker setDate:self.selectedStartAfternoon animated:YES];
        }
        
        [self.datePicker setMinimumDate:self.minAfternoon];
        [self.datePicker setMaximumDate:self.maxAfternoon];
        
        
    }
    if([self.labelSelected isEqualToString:@"endAfternoon"]){
        
        if(self.currentFiche.aprem_fin!=nil){
            [self.datePicker setDate:self.currentFiche.aprem_fin animated:YES]; //selectionné la date (date de la tache)
        }
        else{
            [self.datePicker setDate:self.selectedEndAfternoon animated:YES];
        }
        
        [self.datePicker setMinimumDate:self.minAfternoon];
        [self.datePicker setMaximumDate:self.maxAfternoon];
        
    }

    
}

//Changer le label de la tache suivant datePicker
- (void)changeDateInLabel:(id)sender{
    
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *nomsegue = @"endSelectHours";
    if([segue.identifier isEqualToString:nomsegue]){
        
            selectTrajetsTableViewController* sttvc;
            sttvc = [segue destinationViewController];
            [sttvc setFiches:fiches];
        
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    
    //ON VERIFIE SI AU MOINS UNE PLAGE HORAIRE EST COMPLÈTE//
    if((self.currentFiche.matin_debut==nil || self.currentFiche.matin_fin==nil) && (self.currentFiche.aprem_debut==nil || self.currentFiche.aprem_fin==nil))
    {
        
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Vous devez renseigner au moins une plage horaire" message:@""  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
        
        return NO;
    }
    
    //ON VERIFIE SI IL N'Y A PAS UNE PLAGE HORAIRE DONT IL MANQUE UN CHAMP (EX DEBUT MATIN MAIS PAS FIN MATIN) //
    if((self.currentFiche.matin_debut==nil ^ self.currentFiche.matin_fin==nil) || (self.currentFiche.aprem_debut==nil ^ self.currentFiche.aprem_fin==nil)){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Une plage horaire n'est pas complète" message:@""  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
        
        return NO;
    }
    
    if(self.currentFiche.matin_debut!=nil && self.currentFiche.matin_fin!=nil && [self.currentFiche.matin_debut compare: self.currentFiche.matin_fin] == NSOrderedDescending){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Verifiez les informations données" message:@"L'heure de début du matin doit être inférieur à l'heure de fin du matin"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
        
        return NO;
    }
    if(self.currentFiche.aprem_debut!=nil && self.currentFiche.aprem_fin!=nil && [self.currentFiche.aprem_debut compare: self.currentFiche.aprem_fin] == NSOrderedDescending){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Verifiez les informations données" message:@"L'heure de début de l'après midi doit être inférieur à l'heure de fin de l'après Midi"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
        
        return NO;
    }
    
    if(self.currentFiche.matin_fin!=nil && self.currentFiche.aprem_debut!=nil && [self.currentFiche.matin_fin compare: self.currentFiche.aprem_debut] == NSOrderedDescending){
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Verifiez les informations données" message:@"L'heure de fin du matin doit être inférieur à l'heure de début de l'après Midi"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
        
        return NO;
    }
    
    
    fiches[[NSString stringWithFormat: @"%ld", (long)self.currentIndexFiches]]=self.currentFiche;
    
    
    if(self.currentIndexFiches!=[fiches count]-1){
        if(self.currentIndexFiches==0){
            
            self.firstFiche = self.currentFiche;
            
        }
        self.currentIndexFiches++;
        [self configureView];
        return NO;
    }
    return YES;
}

- (IBAction)prevFiche {
    
    fiches[[NSString stringWithFormat: @"%ld", (long)self.currentIndexFiches]]=self.currentFiche;
    self.currentIndexFiches--;
    [self configureView];

}

//Suppression des claviers
- (IBAction)textFieldReturn:(id)sender {
}
//Suppression des claviers et sous vue
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    
}


- (IBAction)cancelPressed:(id)sender {
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
    [self hideDatePicker];
    
}

- (IBAction)nonePressed:(id)sender {
    
    if([self.labelSelected isEqualToString:@"startMorning"]){
        
        self.currentFiche.matin_debut = nil;
        startMorningLabel.text = @"Modifier";
        
    }
    if([self.labelSelected isEqualToString:@"endMorning"]){
        
        self.currentFiche.matin_fin = nil;
        endMorningLabel.text = @"Modifier";
        
    }
    if([self.labelSelected isEqualToString:@"startAfternoon"]){
        
        self.currentFiche.aprem_debut = nil;
        startAfternoonLabel.text = @"Modifier";
        
    }
    if([self.labelSelected isEqualToString:@"endAfternoon"]){
        
        self.currentFiche.aprem_fin = nil;
        endAfternoonLabel.text = @"Modifier";
        
    }
    
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
    [self hideDatePicker];
    
}

- (IBAction)donePressed:(id)sender {
    [self changeDate];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [self hideDatePicker];
}

-(void)changeDate{
    

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [dateFormatter setLocale:frLocale];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *dateString = [dateFormatter stringFromDate:self.datePicker.date];
    
    
    if(dateString!=nil){
        
        if([self.labelSelected isEqualToString:@"startMorning"]){
            
            self.currentFiche.matin_debut = self.datePicker.date;
            startMorningLabel.text = [NSString stringWithFormat: @"%@",dateString];
            
        }
        if([self.labelSelected isEqualToString:@"endMorning"]){
            
            self.currentFiche.matin_fin = self.datePicker.date;
            endMorningLabel.text = [NSString stringWithFormat: @"%@",dateString];
            
        }
        if([self.labelSelected isEqualToString:@"startAfternoon"]){
            
            self.currentFiche.aprem_debut = self.datePicker.date;
            startAfternoonLabel.text = [NSString stringWithFormat: @"%@",dateString];
            
        }
        if([self.labelSelected isEqualToString:@"endAfternoon"]){
            
            self.currentFiche.aprem_fin = self.datePicker.date;
            endAfternoonLabel.text = [NSString stringWithFormat: @"%@",dateString];
            
        }
    }
    
    
}

- (IBAction)unwindToList:(UIStoryboardSegue *)segue
{
    
    
}



@end
