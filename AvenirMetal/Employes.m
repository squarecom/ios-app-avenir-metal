//
//  Employes.m
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "Employes.h"
#import "Agences.h"
#import "Chefs.h"
#import "LastFiches.h"


@implementation Employes

@dynamic adresse;
@dynamic commentaire;
@dynamic id;
@dynamic interim;
@dynamic nom;
@dynamic prenom;
@dynamic tel;
@dynamic ville;
@dynamic agence_id;
@dynamic chef;
@dynamic fiches;

@end
