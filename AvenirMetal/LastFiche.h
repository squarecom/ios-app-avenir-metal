//
//  Fiche.h
//  AvenirMetal
//
//  Created by jb fuss on 12/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Chantiers.h"
#import "Chefs.h"
#import "Employes.h"


@interface LastFiche : NSManagedObject

@property (nonatomic, retain) NSDate * aprem_debut;
@property (nonatomic, retain) NSDate * aprem_fin;
@property (nonatomic, retain) NSString * commentaire;
@property (nonatomic, retain) NSNumber * granddeplacement;
@property (nonatomic, retain) NSDate * jour;
@property (nonatomic, retain) NSDate * matin_debut;
@property (nonatomic, retain) NSDate * matin_fin;
@property (nonatomic, retain) NSNumber * repas;
@property (nonatomic, retain) NSNumber * trajet;
@property (nonatomic, retain) Chantiers *chantiers_id;
@property (nonatomic, retain) Chefs *chefs_id;
@property (nonatomic, retain) Employes *employe_id;

@end
