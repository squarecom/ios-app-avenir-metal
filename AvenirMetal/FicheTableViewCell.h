//
//  FicheTableViewCell.h
//  AvenirMetal
//
//  Created by jb fuss on 13/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FicheTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameEmployeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *morningHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *afternoonHoursLabel;
@property (weak, nonatomic) IBOutlet UIImageView *deplacementImage;
@property (weak, nonatomic) IBOutlet UIImageView *trajet;

@property (weak, nonatomic) IBOutlet UIImageView *repasImage;

@end
