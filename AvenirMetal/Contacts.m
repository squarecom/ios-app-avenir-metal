//
//  Contacts.m
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "Contacts.h"


@implementation Contacts

@dynamic adresse;
@dynamic commentaire;
@dynamic email;
@dynamic id;
@dynamic nom;
@dynamic prenom;
@dynamic statut;
@dynamic tel;
@dynamic ville;

@end
