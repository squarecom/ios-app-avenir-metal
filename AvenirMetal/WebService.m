//
//  WebService.m
//  AvenirMetal
//
//  Created by jb fuss on 06/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "WebService.h"
#import "Data.h"


@implementation WebService

@synthesize queue;


#pragma mark - Instance du singletton
/**
 * Retourne l'instance partagee du singletton
 *
 * @return WebService Instance statique de la classe
 */
+ (WebService *)sharedInstance
{
    static WebService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[WebService alloc] init];
        sharedInstance.queue = [[NSOperationQueue alloc] init];
    });
    return sharedInstance;
}

#pragma mark - User Methods
- (void) postAuthentification:(id)delegate withLogin:(NSString*)login andPassword:(NSString*)password {
    
    // Définition de la deuxieme partie de l'url grâce aux champs login et password
    NSString *parameters = [NSString stringWithFormat: @"/%@/%@", login, password];
    
    // Réunion de la string de base et de la deuxieme partie
    NSString *urlWithoutParameters = [NSString stringWithFormat:@"%@%@", BASE_URL, AUTH_URL];
    NSString *urlString = [urlWithoutParameters stringByAppendingString:parameters];
   
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //Ligne à décommenter si utilisation du serveur web, à commenter pour serveur local
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
       
        if(delegate != nil && [delegate respondsToSelector:@selector(wsPostAuthentificationCallBack:)])
        {
            [delegate wsPostAuthentificationCallBack:responseObject];
        }
       
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [delegate wsPostAuthentificationConnexionFailed];
        
    }];
    
    [operation start];

	
}

- (void) getListeChantiersByChefChantier:(id)delegate withChef:(NSString*)idChef {
    
    // Définition de la deuxieme partie de l'url grâce aux champs login et password
    NSString *parameters = [NSString stringWithFormat: @"/%@", idChef];
    
    // Réunion de la string de base et de la deuxieme partie
    NSString *urlWithoutParameters = [NSString stringWithFormat:@"%@%@", BASE_URL, CHANTIERS_BY_CHEF_URL];
    
    NSString *urlString = [urlWithoutParameters stringByAppendingString:parameters];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //Ligne à décommenter si utilisation du serveur web, à commenter pour serveur local
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(delegate != nil && [delegate respondsToSelector:@selector(wsPostGetChantiersCallBack:)])
        {
            [delegate wsPostGetChantiersCallBack:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // si la requete n'a pas fonctionné, on explique que ca peut venir soit d'un oubli d'un champ, soit d'un probleme de connexion
        [delegate wsPostGetChantiersFailed];
    }];
    
    [operation start];
    
	
}

- (void) getLastFichesByChantierAndChef:(id)delegate withChantier:(NSString*)idChantier withChef:(NSString*)idChef{
    
    // Définition de la deuxieme partie de l'url grâce aux champs login et password
    NSString *parameters = [NSString stringWithFormat: @"/%@/%@", idChantier,idChef];
    
    // Réunion de la string de base et de la deuxieme partie
    NSString *urlWithoutParameters = [NSString stringWithFormat:@"%@%@", BASE_URL, LAST_FICHES_BY_CHANTIER_AND_CHEF_URL];
    
    NSString *urlString = [urlWithoutParameters stringByAppendingString:parameters];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //Ligne à décommenter si utilisation du serveur web, à commenter pour serveur local
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(delegate != nil && [delegate respondsToSelector:@selector(wsPostGetFichesCallBack:)])
        {
            [delegate wsPostGetFichesCallBack:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [delegate wsPostGetFichesFailed];
    }];
    
    [operation start];
    
	
}

-(void) getListeEmployes:(id)delegate {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, EMPLOYEES_URL];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //Ligne à décommenter si utilisation du serveur web, à commenter pour serveur local
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(delegate != nil && [delegate respondsToSelector:@selector(wsPostGetEmployesCallBack:)])
        {
            [delegate wsPostGetEmployesCallBack:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // si la requete n'a pas fonctionné, on explique que ca peut venir soit d'un oubli d'un champ, soit d'un probleme de connexion
        [delegate wsPostGetEmployesFailed];
    }];
    
    [operation start];

}

-(void) getListeInterimaires:(id)delegate {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, INTERIMS_URL];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //Ligne à décommenter si utilisation du serveur web, à commenter pour serveur local
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(delegate != nil && [delegate respondsToSelector:@selector(wsPostGetInterimairesCallBack:)])
        {
            [delegate wsPostGetInterimairesCallBack:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // si la requete n'a pas fonctionné, on explique que ca peut venir soit d'un oubli d'un champ, soit d'un probleme de connexion
        [delegate wsPostGetInterimsFailed];
    }];
    
    [operation start];
}

-(void) getListeAdministration:(id)delegate {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, ADMINISTRATION_URL];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //Ligne à décommenter si utilisation du serveur web, à commenter pour serveur local
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(delegate != nil && [delegate respondsToSelector:@selector(wsPostGetListeAdministrationCallBack:)])
        {
            [delegate wsPostGetListeAdministrationCallBack:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        // si la requete n'a pas fonctionné, on explique que ca peut venir soit d'un oubli d'un champ, soit d'un probleme de connexion
        [delegate wsPostGetContactsFailed];
        
    }];
    
    [operation start];
    }

-(void) addFiche:(id)delegate withJsonDictionnary:(NSMutableDictionary *)jsonDict{
    
   
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, ADD_FICHE_URL];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:200];
    
    //Ligne à décommenter si utilisation du serveur web, à commenter pour serveur local
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:urlString
       parameters:jsonDict
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              if(delegate != nil && [delegate respondsToSelector:@selector(wsPostAddFiche:)])
              {
           
                [delegate wsPostAddFiche:responseObject];
              }
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"%@",error);
              [delegate wsConnexionFailed];
          }];

    
    //[operation start];
}


#pragma mark - Surcharge AFNetworking
- (void) updateOperationManager:(AFHTTPRequestOperationManager *) manager {
    [manager.responseSerializer setAcceptableContentTypes: [NSSet setWithObjects:@"application/json", @"text/json", @"text/html", @"text/plain", @"text/javascript", nil]];
}



@end
