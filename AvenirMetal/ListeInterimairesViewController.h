//
//  ListeInterimairesViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 21/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface ListeInterimairesViewController : UITableViewController<WebServiceDelegate,UISearchBarDelegate, UISearchDisplayDelegate>

    @property (strong, nonatomic) IBOutlet UITableView *tableview;
    @property(nonatomic,strong) NSMutableSet *employesSelect;
    @property(nonatomic,assign) NSInteger numberEmployes;
    @property (strong, nonatomic)NSMutableSet *fiches;
    @property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
