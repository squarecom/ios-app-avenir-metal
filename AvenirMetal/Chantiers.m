//
//  Chantiers.m
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "Chantiers.h"
#import "Assignations_chantier.h"
#import "LastFiches.h"


@implementation Chantiers

@dynamic adresse;
@dynamic clos;
@dynamic commentaire;
@dynamic created;
@dynamic id;
@dynamic nom;
@dynamic num;
@dynamic tel;
@dynamic ville;
@dynamic assignation;
@dynamic fiches;

@end
