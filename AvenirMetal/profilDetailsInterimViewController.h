//
//  profilDetailsInterimViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 09/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface profilDetailsInterimViewController : UITableViewController
@property (strong, nonatomic) NSDictionary* detailsInterim;
- (IBAction)callNumber:(id)sender;
- (IBAction)callNumberAgence:(id)sender;

@end
