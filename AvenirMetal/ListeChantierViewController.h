//
//  ListeChantierViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 27/04/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface ListeChantierViewController : UITableViewController<WebServiceDelegate,UISearchBarDelegate, UISearchDisplayDelegate>
@property(strong, nonatomic) NSString * login;
@property(strong, nonatomic) NSString * idchef;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSString *numchantier;
@property (strong, nonatomic) NSString *nomchantier;
@end
