//
//  selectRepasGransDeplacementTableViewController.m
//  AvenirMetal
//
//  Created by jb fuss on 07/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "selectRepasGransDeplacementTableViewController.h"
#import "ficheObject.h"
#import "RepasDeplacementTableViewCell.h"
#import "RecapFichesTableViewController.h"

@interface selectRepasGransDeplacementTableViewController ()
    @property(atomic,strong) NSArray *pickerViewValues;
    @property (nonatomic, strong) UIGestureRecognizer *tapper; //Objet gestion évènement pour desactiver le clavier
    @property(atomic,strong) UIPickerView *pickerView;
    @property (nonatomic,strong)UIActionSheet *pickerAction;
    @property(atomic,assign) NSInteger rowSelected;

    /*
    *  System Versioning Preprocessor Macros
    */

    #define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
    #define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
    #define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
    #define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
    #define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


@end

@implementation selectRepasGransDeplacementTableViewController
@synthesize fiches;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pickerViewValues = [[NSArray alloc]         initWithObjects:@"Aucun",@"Repas",@"Grand deplacement", nil];
    self.tapper = [[UITapGestureRecognizer alloc]
                   initWithTarget:self action:@selector(handleSingleTap:)];//Event simple touché
    self.tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:self.tapper];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [fiches count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RepasDeplacementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RepasDeplacementCell" forIndexPath:indexPath];
    
    // Configure the cell...
   ficheObject *fiche = [fiches objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    
    cell.nameEmployeeCell.text = fiche.name_employee_output;
    
    
    if(fiche.repas!=nil || fiche.granddeplacement!=nil){
        if(fiche.repas==[NSNumber numberWithInt:1]){
            cell.choiceCell.text=@"Repas";
        }else{
            if(fiche.granddeplacement==[NSNumber numberWithInt:1]){
                cell.choiceCell.text=@"Grand deplacement";
            }else{
                cell.choiceCell.text = @"Aucun";
            }
        }
    }else{
        fiche.repas=[NSNumber numberWithInt:0];
        fiche.granddeplacement = [NSNumber numberWithInt:0];
        
        fiches[[NSString stringWithFormat: @"%ld", (long)indexPath.row]]=fiche;
        
        cell.choiceCell.text = @"Aucun";
    }
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath   *)indexPath
{
    UITableViewCell * theCell = (UITableViewCell *)[tableView
                                                    cellForRowAtIndexPath:indexPath];
    
    CGPoint tableViewCenter = [tableView contentOffset];
    tableViewCenter.y += self.tableView.frame.size.height;
    
    [self.tableView setContentOffset:CGPointMake(0,theCell.center.y-100) animated:YES];
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor blueColor];
    theCell.selectedBackgroundView = v;
    self.rowSelected = indexPath.row;
    
    
    ficheObject *fiche = [fiches objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
   

    
    //display datePicker for ios 7
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        
        [self displayPickerIos7:fiche.name_employee_output];
        
    }else{
        //display for ios 8
        [self displayPickerIos8:fiche.name_employee_output];
    }

}


- (void) displayPickerIos7:(NSString *)name{
    
    [[[UIAlertView alloc] initWithTitle:name
                                message:@"Repas ou Grand Déplacement"
                               delegate:self
                      cancelButtonTitle:@"Aucun"
                      otherButtonTitles:@"Repas", @"Grand déplacement", nil] show];
    
    
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //FOR IOS 7
    [self changeValue:buttonIndex];
}

- (void) displayPickerIos8:(NSString *)name{
    
    UIAlertController *alert=   [UIAlertController
                                 alertControllerWithTitle:name
                                 message:@"Repas ou Grand Déplacement"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alert.view setBounds:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //
    
    UIAlertAction *noneAction = [UIAlertAction actionWithTitle:@"Aucun" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                 {
                                     [self changeValue:0];
                                     [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                                 }];
    
    UIAlertAction *repasAction = [UIAlertAction actionWithTitle:@"Repas" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      [self changeValue:1];
                                      [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                                  }];
    
    UIAlertAction *deplacementAction = [UIAlertAction actionWithTitle:@"Grand déplacement" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                        {
                                            [self changeValue:2];
                                            [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
                                            
                                        }];
    
    
    
    
    [alert addAction:noneAction];
    [alert addAction:repasAction];
    [alert addAction:deplacementAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    
   
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = 3;
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    title = [self.pickerViewValues objectAtIndex:row];
    
    return title;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    
   // self.pickerView.hidden = YES;
    //[self.view endEditing:YES];
}

- (void)cancelActionPickerView:(id)sender{
    
    [self.pickerAction dismissWithClickedButtonIndex:0 animated:YES];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [self.view endEditing:YES];
    [self.tableView reloadData];
    
}
- (void)doneActionPickerView:(id)sender{
    
    //[self changeValue];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    NSInteger row = [self.pickerView selectedRowInComponent:0];
    [self changeValue:row];
    [self.pickerAction dismissWithClickedButtonIndex:0 animated:YES];
    [self.view endEditing:YES];
    
}

-(void)changeValue:(NSInteger)row{
    //NSInteger row = [self.pickerView selectedRowInComponent:0];
    ficheObject *fiche = [fiches objectForKey:[NSString stringWithFormat:@"%ld", (long)self.rowSelected]];
    
    switch (row) {
        case 0: //AUCUN
            fiche.repas=[NSNumber numberWithInt:0];
            fiche.granddeplacement = [NSNumber numberWithInt:0];
            break;
            
        case 1: //REPAS
            fiche.repas=[NSNumber numberWithInt:1];
            fiche.granddeplacement = [NSNumber numberWithInt:0];
            break;
            
        case 2: //Grd deplacement
            fiche.repas=[NSNumber numberWithInt:0];
            fiche.granddeplacement = [NSNumber numberWithInt:1];
            break;
            
        default:
            break;
    }
    
    
    fiches[[NSString stringWithFormat: @"%ld", (long)self.rowSelected]]=fiche;
    
    [self.tableView reloadData];
    
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *nomsegue = @"endSelectRepasGrandDeplacement";
    if([segue.identifier isEqualToString:nomsegue]){
        
        RecapFichesTableViewController* rftvc;
        rftvc = [segue destinationViewController];
        [rftvc setFiches:fiches];
        
    }
}

@end
