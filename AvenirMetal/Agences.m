//
//  Agences.m
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "Agences.h"
#import "Employes.h"


@implementation Agences

@dynamic adresse;
@dynamic commentaire;
@dynamic email;
@dynamic id;
@dynamic nom;
@dynamic tel;
@dynamic ville;
@dynamic employees;

@end
