//
//  AppDelegate.m
//  AvenirMetal
//
//  Created by jb fuss on 17/04/14.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "fiche.h"
#import "WebService.h"

@implementation AppDelegate

@synthesize managedObjectContextApi = _managedObjectContextApi;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;


/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    // registering for remote notifications
    
    [self registerForRemoteNotification];
    // Override point for customization after application launch.
    // Handle launching from a notification
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( ![userDefaults valueForKey:@"version"] )
    {
        // Premier lancement => on ajoute les notifs ooublie ajout fiches
        [self deleteNotifications];
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        
        NSDate *pickerDate = [NSDate date];
        
        NSDateComponents *componentsForFireDate = [calendar components:(NSCalendarUnitYear | NSCalendarUnitWeekday |  NSCalendarUnitHour | NSCalendarUnitMinute| NSCalendarUnitSecond | NSCalendarUnitWeekday) fromDate: pickerDate];
        
        NSDictionary *userDict = [NSDictionary dictionaryWithObject:@"ficheNotSend" forKey:@"msg"];
        
        [componentsForFireDate setWeekday:2]; //Lundi
        [componentsForFireDate setHour:19];
        [componentsForFireDate setMinute:0];
        [componentsForFireDate setSecond:0];
        
        //Notification Lundi
        
        UILocalNotification* notifMonday = [[UILocalNotification alloc] init];
        notifMonday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifMonday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifMonday.soundName = UILocalNotificationDefaultSoundName;
        notifMonday.userInfo = userDict;
        notifMonday.timeZone = [NSTimeZone defaultTimeZone];
        notifMonday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifMonday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifMonday];
        
        
        [componentsForFireDate setWeekday:3] ; //Mardi
        [componentsForFireDate setHour:19];
        [componentsForFireDate setMinute:0];
        [componentsForFireDate setSecond:0];
        
        //Notification Mardi
        
        UILocalNotification* notifTuesday = [[UILocalNotification alloc] init];
        notifTuesday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifTuesday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifTuesday.userInfo = userDict;
        notifTuesday.soundName = UILocalNotificationDefaultSoundName;
        notifTuesday.timeZone = [NSTimeZone defaultTimeZone];
        notifTuesday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifTuesday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifTuesday];
        
        [componentsForFireDate setWeekday:4] ; //Mercredi
        [componentsForFireDate setHour:19];
        [componentsForFireDate setMinute:0];
        [componentsForFireDate setSecond:0];
        
        //Notification Mercredi
        
        UILocalNotification* notifWednesday = [[UILocalNotification alloc] init];
        notifWednesday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifWednesday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifWednesday.userInfo = userDict;
        notifWednesday.soundName = UILocalNotificationDefaultSoundName;
        notifWednesday.timeZone = [NSTimeZone defaultTimeZone];
        notifWednesday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifWednesday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifWednesday];
        
        [componentsForFireDate setWeekday:5] ; //Jeudi
        [componentsForFireDate setHour:19];
        [componentsForFireDate setMinute:0];
        [componentsForFireDate setSecond:0];
        
        //Notification Jeudi
        
        UILocalNotification* notifThursday = [[UILocalNotification alloc] init];
        notifThursday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifThursday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifThursday.userInfo = userDict;
        notifThursday.soundName = UILocalNotificationDefaultSoundName;
        notifThursday.timeZone = [NSTimeZone defaultTimeZone];
        notifThursday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifThursday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifThursday];
        
        [componentsForFireDate setWeekday:6] ; //Vendredi
        [componentsForFireDate setHour:19];
        [componentsForFireDate setMinute:0];
        [componentsForFireDate setSecond:0];
        
        //Notification Vendredi
        
        UILocalNotification* notifFriday = [[UILocalNotification alloc] init];
        notifFriday.fireDate = [calendar dateFromComponents:componentsForFireDate];
        notifFriday.alertBody = @"Vous avez oublié de remplir une fiche aujourd'hui";
        notifFriday.userInfo = userDict;
        notifFriday.soundName = UILocalNotificationDefaultSoundName;
        notifFriday.timeZone = [NSTimeZone defaultTimeZone];
        notifFriday.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        notifFriday.repeatInterval = NSCalendarUnitWeekday;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifFriday];
        

        
        // Adding version number to NSUserDefaults for first version:
        [userDefaults setFloat:[[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue] forKey:@"version"];
    }
    
    // Override point for customization after application launch.
    return YES;
}

- (void)registerForRemoteNotification {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}
#endif

-(void) deleteNotifications {
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"msg"]];
        if ([uid isEqualToString:@"ficheNotSend"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
        }
    }
    
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    application.applicationIconBadgeNumber = 0;
    
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// 1
- (NSManagedObjectContext *) managedObjectContext {
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContext;
}
- (NSManagedObjectContext *) managedObjectContextApi {
    
    if (_managedObjectContextApi != nil) {
        return _managedObjectContextApi;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContextApi = [[NSManagedObjectContext alloc] init];
        [_managedObjectContextApi setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContextApi;
}

//2
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return _managedObjectModel;
}

//3
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"AvenirMetal.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil URL:storeUrl options:nil error:&error]) {
        /*Error for store creation should be handled in here*/
    }
    
    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

-(NSArray *)getFicheWhichFailed{
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Fiche"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedFiches = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
   
    return fetchedFiches;
}

-(void)deleteFicheWhichFailed:(NSArray *)fetchedFiches{
    
    for (NSManagedObject *managedObject in fetchedFiches) {
    	[self.managedObjectContext deleteObject:managedObject];
    }
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
    
    }
    
}


-(void)addFicheWhichFailed{
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Fiche"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedFiches = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableDictionary *fichesMDforJSON = [[NSMutableDictionary alloc] init];
    int i = 0;
    
    for(Fiche *fiche in fetchedFiches){
        
        NSDateFormatter * dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *jourString =  fiche.jour;
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *startMorning=@"";
        if(fiche.matin_debut!=nil){
            startMorning  = [dateFormatter stringFromDate:fiche.matin_debut];
        }
        
        NSString *endMorning=@"";
        if(fiche.matin_fin!=nil){
            endMorning = [dateFormatter stringFromDate:fiche.matin_fin];
        }
        
        NSString *startAfternoon=@"";
        if(fiche.aprem_debut!=nil){
            startAfternoon = [dateFormatter stringFromDate:fiche.aprem_debut];
        }
        
        NSString *endAfternoon=@"";
        if(fiche.aprem_fin!=nil){
            endAfternoon = [dateFormatter stringFromDate:fiche.aprem_fin];
        }
        
        
        NSDictionary *ficheDict =
        @{
          @"jour" : jourString,
          @"chantiers_has_chefs_chantiers_id" : fiche.chantiers_has_chefs_chantiers_id,
          @"chantiers_has_chefs_chefs_id" : fiche.chantiers_has_chefs_chefs_id,
          @"employe_id" : fiche.employe_id,
          @"matin_debut" : startMorning,
          @"matin_fin" : endMorning,
          @"aprem_debut": startAfternoon,
          @"aprem_fin": endAfternoon,
          @"trajet":fiche.trajet,
          @"repas":fiche.repas,
          @"granddeplacement":fiche.granddeplacement,
          };


        
        fichesMDforJSON[[NSString stringWithFormat: @"%d", i]]=ficheDict;
        
        i++;
    }
    
    [[WebService sharedInstance] addFiche:(id)self withJsonDictionnary:fichesMDforJSON];
    
}

- (void) wsPostAddFiche:(NSDictionary *)responseAdd{
    
    
    
    if([responseAdd[@"success"] isEqualToString:@"true"]){
        
        NSArray* fiches = [self getFicheWhichFailed];
        [self deleteFicheWhichFailed:fiches];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Votre fiche a bien été envoyée"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Une erreur est arrivée"
                                                            message:@"Veuillez ressayer"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }
    
}

- (void) wsConnexionFailed{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion"
                                                        message:@"La fiche a été enregistrée - Elle sera renvoyée plus tard"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    
    [alertView show];
}



@end
