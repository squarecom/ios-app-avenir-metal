//
//  contactAdminTableViewCell.m
//  AvenirMetal
//
//  Created by jb fuss on 13/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "contactAdminTableViewCell.h"

@implementation contactAdminTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
