//
//  Chantiers.h
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Assignations_chantier, LastFiches;

@interface Chantiers : NSManagedObject

@property (nonatomic, retain) NSString * adresse;
@property (nonatomic, retain) NSNumber * clos;
@property (nonatomic, retain) NSString * commentaire;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * nom;
@property (nonatomic, retain) NSString * num;
@property (nonatomic, retain) NSString * tel;
@property (nonatomic, retain) NSString * ville;
@property (nonatomic, retain) Assignations_chantier *assignation;
@property (nonatomic, retain) NSSet *fiches;
@end

@interface Chantiers (CoreDataGeneratedAccessors)

- (void)addFichesObject:(LastFiches *)value;
- (void)removeFichesObject:(LastFiches *)value;
- (void)addFiches:(NSSet *)values;
- (void)removeFiches:(NSSet *)values;

@end
