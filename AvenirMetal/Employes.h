//
//  Employes.h
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Agences, Chefs, LastFiches;

@interface Employes : NSManagedObject

@property (nonatomic, retain) NSString * adresse;
@property (nonatomic, retain) NSString * commentaire;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * interim;
@property (nonatomic, retain) NSString * nom;
@property (nonatomic, retain) NSString * prenom;
@property (nonatomic, retain) NSString * tel;
@property (nonatomic, retain) NSString * ville;
@property (nonatomic, retain) Agences *agence_id;
@property (nonatomic, retain) Chefs *chef;
@property (nonatomic, retain) NSSet *fiches;
@end

@interface Employes (CoreDataGeneratedAccessors)

- (void)addFichesObject:(LastFiches *)value;
- (void)removeFichesObject:(LastFiches *)value;
- (void)addFiches:(NSSet *)values;
- (void)removeFiches:(NSSet *)values;

@end
