//
//  ListeEmployesTableViewController.h
//  AvenirMetal
//
//  Created by jb fuss on 20/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface ListeEmployesTableViewController : UITableViewController<WebServiceDelegate,UISearchBarDelegate, UISearchDisplayDelegate>

    -(void)setNumberEmployesToSelect:(NSInteger)numberEmployes;
    @property (strong, nonatomic) IBOutlet UITableView *tableview;
    @property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
    @property (strong, nonatomic)NSMutableSet *fiches;

@end
