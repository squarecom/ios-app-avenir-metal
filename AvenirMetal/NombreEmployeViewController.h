//
//  NombreEmployeViewController.h
//  AvenirMetal
//
//  Created by jb fuss on 20/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NombreEmployeViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>
    @property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
    @property (strong, nonatomic)NSMutableSet *fiches;
    - (void)configureView;
@end
