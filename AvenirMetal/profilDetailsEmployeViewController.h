//
//  profilDetailsEmployeViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 09/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface profilDetailsEmployeViewController : UITableViewController<WebServiceDelegate>
@property (strong, nonatomic) NSDictionary* detailsEmploye;
- (IBAction)callNumber:(id)sender;

@end
