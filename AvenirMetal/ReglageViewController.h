//
//  ReglageViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 10/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReglageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISwitch *switchAlarm;

- (IBAction)saveReglages:(id)sender;
- (IBAction)switchChanged:(id)sender;


@end
