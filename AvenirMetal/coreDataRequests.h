//
//  coreDataRequests.h
//  AvenirMetal
//
//  Created by jb fuss on 14/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Chefs.h"

@interface coreDataRequests : NSObject

    @property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

    //CHEF FCTS //
    -(void) updateChef:(NSDictionary *)chefDict withPassword:(NSString *) password;
    -(NSArray *) getChef:(NSDictionary *)chefDict;
    -(Chefs *)getChefLog;

    // CHANTIER FCTS
    -(void) updateChantier:(NSArray *)chantiersArray;
    -(NSArray *) getChantiers;

    // EMPLOYE FCTS
    -(void) updateEmployesNotInterim:(NSArray *)employesArray;
    -(NSArray *) getEmployesNotInterim;

    //INTERIM FCTS
    -(void) updateInterims:(NSArray *)employesArray;
    -(NSArray *) getInterims;

    //CONTACTS FCTS
    -(void) updateContacts:(NSArray *)contactsArray;
    -(NSArray *) getContacts;

    //FICHES FCTS
    -(void) updateFiches:(NSArray *)fichesArray;
    -(NSArray *) getFichesByChantierAndChef:(NSString *)chantier_id withChef:(NSString *)chef_id;

    -(BOOL) getIfFichesByChantierChefJour:(NSString *)chantier_id withChef:(NSString *)chef_id withJour:(NSString *)jour;

@end
