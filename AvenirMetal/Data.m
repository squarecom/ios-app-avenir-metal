//
//  Data.m
//  AvenirMetal
//
//  Created by jb fuss on 07/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "Data.h"

@implementation Data

    NSString * const BASE_URL = @"http://www.alpex-epdm.fr/AM/api/";
    NSString * const AUTH_URL = @"getChef";
    NSString * const CHANTIER_BY_ID_URL = @"getChantier";
    NSString * const CHANTIERS_BY_CHEF_URL = @"getChantiersByChef";
    NSString * const LAST_FICHES_BY_CHANTIER_URL = @"getLastFichesByChantier";
    NSString * const LAST_FICHES_BY_CHANTIER_AND_CHEF_URL = @"getLastFichesByChantierAndChef";
    NSString * const EMPLOYE_BY_ID_URL = @"getChef";
    NSString * const EMPLOYEES_URL = @"getEmployees";
    NSString * const INTERIMS_URL = @"getInterims";
    NSString * const ADMINISTRATION_URL = @"getContactAdmin";
    NSString * const ADD_FICHE_URL = @"setFiche";
    NSString * CHANTIER_SELECTED = @"";
    NSString * CHEF_LOG= @"";
    NSDictionary * CHANTIER_SELECTED_INFO;
    NSString * CHEF_LOG_INFO=@"";
    NSDate *date_notif = nil;

@end
