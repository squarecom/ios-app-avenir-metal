//
//  profilDetailsAdministrationViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 11/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface profilDetailsAdministrationViewController : UITableViewController
- (IBAction)callNumber:(id)sender;
@property (strong, nonatomic) NSDictionary* detailsAdministration;
@end
