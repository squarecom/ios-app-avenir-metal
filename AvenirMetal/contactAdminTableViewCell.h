//
//  contactAdminTableViewCell.h
//  AvenirMetal
//
//  Created by jb fuss on 13/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface contactAdminTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellName;
@property (weak, nonatomic) IBOutlet UILabel *cellStatut;

@end
