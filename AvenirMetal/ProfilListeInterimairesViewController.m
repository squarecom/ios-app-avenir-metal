//
//  ProfilListeInterimairesViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 21/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "ProfilListeInterimairesViewController.h"
#import "profilDetailsInterimViewController.h"
#import "coreDataRequests.h"

@interface ProfilListeInterimairesViewController ()

    @property (strong) NSArray* interimaires;
    @property (strong, nonatomic) profilDetailsInterimViewController *pdivc;
    @property (nonatomic,assign) UIActivityIndicatorView *spinner;
    @property (strong, nonatomic) NSArray *searchResults;
    @property (nonatomic, assign) BOOL isSearch;
    @property (strong, nonatomic) NSMutableArray *mutableInterimaires;
    @property (strong, nonatomic) coreDataRequests *requests;

@end

@implementation ProfilListeInterimairesViewController
@synthesize searchBar;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.requests = [[coreDataRequests alloc]init];
    [self startSpinner];
    
    searchBar.delegate = (id)self;
    searchBar.showsCancelButton = YES;
    [searchBar setShowsCancelButton:YES animated:YES];
    
    [[WebService sharedInstance] getListeInterimaires:(id)self];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) wsPostGetInterimairesCallBack:(NSDictionary *)interimairesDict {
    [self.spinner stopAnimating];
    
    self.interimaires = (NSArray *)interimairesDict;
    
    self.mutableInterimaires = [NSMutableArray arrayWithArray:self.interimaires];
    
    [self.requests updateInterims:self.interimaires];
    
    [self.tableView reloadData];
}


- (void) wsPostGetInterimsFailed{
    
    [self.spinner stopAnimating];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Assurez vous d'avoir une connexion internet"
                                                        message:@"Les données peuvent ne pas être à jour"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
    
    self.interimaires = [self.requests getInterims];
    
    self.mutableInterimaires = [NSMutableArray arrayWithArray:self.interimaires];
    
    [self.tableView reloadData];
    
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.isSearch) {
        return [self.searchResults count];
        
    } else {
        return [self.interimaires count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"interimcell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSDictionary *interimairesDetail;
    if (self.isSearch) {
        interimairesDetail  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        interimairesDetail = [self.interimaires objectAtIndex:indexPath.row];
    }
    
    
    cell.textLabel.text = [NSString stringWithFormat: @"%@ %@",interimairesDetail[@"Employe"][@"nom"],interimairesDetail[@"Employe"][@"prenom"]];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *interimaireDetails;
    if (self.isSearch) {
        interimaireDetails  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        interimaireDetails = [self.interimaires objectAtIndex:indexPath.row];
    }
    
    self.pdivc.detailsInterim = interimaireDetails;
    
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"detailsInterimSegue"]){
        
        //On recupere le destination view controller
        self.pdivc = [segue destinationViewController];
        
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        self.isSearch = FALSE;
        
    }
    else
    {
        self.isSearch = TRUE;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Employe.nom contains[cd] %@ || SELF.Employe.prenom contains[cd] %@  ",
                                        text,text];
        
        self.searchResults = [self.mutableInterimaires filteredArrayUsingPredicate:resultPredicate];
        
    }
    [self.tableView reloadData];
}

-(void) searchBarSearchButtonClicked:(UISearchBar*) searchBarObject {
    //do some thing
    [self.view endEditing:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBarObject
{
    searchBar.text=@"";
    self.isSearch = FALSE;
    [self.tableView reloadData];
    [self.view endEditing:YES];
}


-(void)startSpinner{
    UIActivityIndicatorView *spinnerObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = spinnerObject;
    self.spinner.center = CGPointMake(160, 240);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
