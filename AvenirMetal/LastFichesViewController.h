//
//  LastFichesViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 02/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface LastFichesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,WebServiceDelegate>
    @property(strong, nonatomic) NSDictionary * chantier;
    @property(strong, nonatomic) NSString * nomchantier;
    @property (weak, nonatomic) IBOutlet UITableView *tableView;

    -(void)configureView;

@end
