//
//  RecapFichesTableViewController.h
//  AvenirMetal
//
//  Created by jb fuss on 07/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface RecapFichesTableViewController : UITableViewController<WebServiceDelegate>
    @property (weak, nonatomic) NSMutableDictionary *fiches;
@end
