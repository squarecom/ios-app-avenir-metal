//
//  Assignations_chantier.h
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Chantiers, Chefs;

@interface Assignations_chantier : NSManagedObject

@property (nonatomic, retain) Chantiers *chantier;
@property (nonatomic, retain) NSSet *chef;
@end

@interface Assignations_chantier (CoreDataGeneratedAccessors)

- (void)addChefObject:(Chefs *)value;
- (void)removeChefObject:(Chefs *)value;
- (void)addChef:(NSSet *)values;
- (void)removeChef:(NSSet *)values;

@end
