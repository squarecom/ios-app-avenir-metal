//
//  LastFichesParticipantsViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 02/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface LastFichesParticipantsViewController : UITableViewController<WebServiceDelegate>
    @property (strong, nonatomic) NSDictionary * fichejour;
    @property (strong, nonatomic) NSDictionary *chantier;
    
@end
