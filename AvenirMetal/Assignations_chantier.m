//
//  Assignations_chantier.m
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "Assignations_chantier.h"
#import "Chantiers.h"
#import "Chefs.h"


@implementation Assignations_chantier

@dynamic chantier;
@dynamic chef;

@end
