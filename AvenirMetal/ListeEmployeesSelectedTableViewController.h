//
//  ListeEmployeesSelectedTableViewController.h
//  AvenirMetal
//
//  Created by jb fuss on 07/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListeEmployeesSelectedTableViewController : UITableViewController

    @property (strong, nonatomic) NSArray *employeesSelected;
    @property (strong, nonatomic)NSMutableSet *lastFiches;

@end
