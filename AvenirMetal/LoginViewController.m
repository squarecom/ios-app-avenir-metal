//
//  LoginViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 27/04/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "LoginViewController.h"
#import "ListeChantierViewController.h"
#import "AFNetworking.h"
#import "Data.h"
#import "AppDelegate.h"
#import "Fiche.h"
#import "Chefs.h"
#import "coreDataRequests.h"

CGFloat animatedDistance;

@interface LoginViewController ()

    @property (strong, nonatomic) IBOutlet UITextField *tflogin;
    @property (strong, nonatomic) IBOutlet UITextField *tfpassword;
    @property (strong) NSDictionary* infologin;
    @property (strong, nonatomic) NSString * login;
    @property (strong, nonatomic)NSString * idchef;
    @property (nonatomic,assign) UIActivityIndicatorView *spinner;
    @property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
    @property (nonatomic,strong)NSArray *fetchedListeFailed;
    @property (nonatomic,strong) AppDelegate *appDelegate;
    @property (nonatomic,strong) coreDataRequests *requests;


@end

@implementation LoginViewController


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Sortir du clavier quand on clique n'importe ou sur l'écran
    [[self view] endEditing:YES];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tfpassword.secureTextEntry = YES;//Change field in password field
    
  // self.tflogin.text = @"a.richard";
    //self.tfpassword.text = @"avenir38";
    self.tflogin.delegate = self;
    self.tfpassword.delegate = self;
    self.navigationItem.hidesBackButton = YES;
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = self.appDelegate.managedObjectContext;
    self.requests = [[coreDataRequests alloc]init];
    
    
    self.fetchedListeFailed = [self.appDelegate getFicheWhichFailed];
    
    if([self.fetchedListeFailed count]>0){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Voulez vous envoyez les fiches existantes ?" message:@"" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui", nil];
        [alert show];

        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){ //envois des fiches
        [self startSpinner];
        [self addFicheWhichFailed];
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)settingsTapped:(id)sender {
    
}

- (IBAction)connecTapped:(id)sender
{
    [self startSpinner];
    
    [[WebService sharedInstance] postAuthentification:(id)self withLogin:self.tflogin.text andPassword:self.tfpassword.text];

}

#pragma mark - WebServices Methods
- (void) wsPostAuthentificationCallBack:(NSDictionary *)authDict {
    [self.spinner stopAnimating];
    
    self.infologin = authDict;
      
    // Test si l'objet récupéré est null (Authentification non réussie) ou rempli (authentificiation réussie)
    if(self.infologin != NULL){
        //Si l'authentif est réussie, on charge la nouvelle vue grâce à un perform segue et on sauve le login
        self.login = self.infologin[@"Chef"][@"username"];
        self.idchef = self.infologin[@"Chef"][@"id"];
        
       [self.requests updateChef:authDict withPassword:self.tfpassword.text];
        
        [self performSegueWithIdentifier:@"loginsegue" sender:self];
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion"
                                                            message:@"Identifiant ou Mot de Passe incorrect"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }
    
    
}

-(void)wsPostAuthentificationConnexionFailed{
    
    [self.spinner stopAnimating];
    
    NSDictionary *chefDict =
    @{
      @"username" : self.tflogin.text,
      @"password" : self.tfpassword.text,
      
      };
    
    NSArray *fetchedChef = [self.requests getChef:chefDict];
    
    if([fetchedChef count]!=0){
        
        Chefs *chef = [fetchedChef objectAtIndex:0];
        
        self.login = chef.username;
        self.idchef = [chef.id stringValue];
        
        [self performSegueWithIdentifier:@"loginsegue" sender:self];
        
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Erreur lors de la connexion à internet"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
   }
    
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSString *nomsegue = @"loginsegue";
    if([segue.identifier isEqualToString:nomsegue]){
        
        CHEF_LOG = self.idchef;
        CHEF_LOG_INFO = self.login;
        
    
    }
    
}

-(void)startSpinner{
    UIActivityIndicatorView *spinnerObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = spinnerObject;
    self.spinner.center = CGPointMake(160, 240);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

-(void)addFicheWhichFailed{
    
    // Query on managedObjectContext With Generated fetchRequest
    NSMutableDictionary *fichesMDforJSON = [[NSMutableDictionary alloc] init];
    int i = 0;
    
    for(Fiche *fiche in self.fetchedListeFailed){
        
        NSDateFormatter * dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *jourString =  fiche.jour;
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *startMorning=@"";
        if(fiche.matin_debut!=nil){
            startMorning  = [dateFormatter stringFromDate:fiche.matin_debut];
        }
        
        NSString *endMorning=@"";
        if(fiche.matin_fin!=nil){
            endMorning = [dateFormatter stringFromDate:fiche.matin_fin];
        }
        
        NSString *startAfternoon=@"";
        if(fiche.aprem_debut!=nil){
            startAfternoon = [dateFormatter stringFromDate:fiche.aprem_debut];
        }
        
        NSString *endAfternoon=@"";
        if(fiche.aprem_fin!=nil){
            endAfternoon = [dateFormatter stringFromDate:fiche.aprem_fin];
        }
        
        
        NSDictionary *ficheDict =
        @{
          @"jour" : jourString,
          @"chantiers_has_chefs_chantiers_id" : fiche.chantiers_has_chefs_chantiers_id,
          @"chantiers_has_chefs_chefs_id" : fiche.chantiers_has_chefs_chefs_id,
          @"employe_id" : fiche.employe_id,
          @"matin_debut" : startMorning,
          @"matin_fin" : endMorning,
          @"aprem_debut": startAfternoon,
          @"aprem_fin": endAfternoon,
          @"trajet":fiche.trajet,
          @"repas":fiche.repas,
          @"granddeplacement":fiche.granddeplacement,
          };
        

        fichesMDforJSON[[NSString stringWithFormat: @"%d", i]]=ficheDict;
        
        i++;
    }
    
    [[WebService sharedInstance] addFiche:(id)self withJsonDictionnary:fichesMDforJSON];
    
}

- (void) wsPostAddFiche:(NSDictionary *)responseAdd{
    
    [self.spinner stopAnimating];
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"msg"]];
        if ([uid isEqualToString:@"failedSendFiche"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
        }
    }
    
    
    if([responseAdd[@"success"] isEqualToString:@"true"]){
        
        [self.appDelegate deleteFicheWhichFailed:self.fetchedListeFailed];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Votre fiche a bien été envoyée"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Une erreur est arrivée"
                                                            message:@"Veuillez ressayer"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
    }
    
}

- (void) wsConnexionFailed{
    
    [self.spinner stopAnimating];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Erreur de connexion"
                                                        message:@"La fiche a été enregistrée - Elle sera renvoyée plus tard"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    
    [alertView show];
}

//Declare a delegate, assign your textField to the delegate and then include these methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,320,460)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,320,460)];
}

@end
