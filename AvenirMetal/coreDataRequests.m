//
//  coreDataRequests.m
//  AvenirMetal
//
//  Created by jb fuss on 14/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "coreDataRequests.h"
#import "AppDelegate.h"
#import "Chefs.h"
#import "Employes.h"
#import "Data.h"
#import "Chantiers.h"
#import "Assignations_chantier.h"
#import "Agences.h"
#import "Contacts.h"
#import "LastFiches.h"

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

@implementation coreDataRequests
@synthesize managedObjectContext;

// CHEF FCTS
-(void) updateChef:(NSDictionary *)chefDict withPassword:(NSString *) password{
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Chefs"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id==%d", [chefDict[@"Chef"][@"id"] intValue]];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedChef = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Chefs *chef;
    
    if([fetchedChef count]==1){
        
        chef = [fetchedChef objectAtIndex:0];
        
    }else{
        
        chef = [NSEntityDescription insertNewObjectForEntityForName:@"Chefs" inManagedObjectContext:self.managedObjectContext];
        
    }
    
    
    [chef setId:@([chefDict[@"Chef"][@"id"] intValue])];
    [chef setPassword:password];
    [chef setUsername:chefDict[@"Chef"][@"username"]];
    
    // GET EMPLOYE //
    
    Employes *employe  = [self addChef:chefDict withChef:chef];
    
    /// SET EMPLOYE ///
    [chef setEmploye_id:employe];
    
    // SAVE //
    if (![managedObjectContext save:&error]) {
        //Handle any error with the saving of the context
    }
}

-(NSArray *) getChef:(NSDictionary *)chefDict{
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Chefs"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@) AND (password==%@)", chefDict[@"username"],chefDict[@"password"]];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedChef = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return fetchedChef;
}

-(Chefs *)getChefLog{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Chefs"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id==%d",[CHEF_LOG intValue]];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedChef = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Chefs *chefLog = [fetchedChef objectAtIndex:0];
    
    return chefLog;
}

-(Employes *) addChef:(NSDictionary *)employeDict withChef:(Chefs *)chef{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Employes"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id==%d", [employeDict[@"Employe"][@"id"] intValue]];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedEmploye = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Employes* employe;
    
    if([fetchedEmploye count]==1){
        
        employe = [fetchedEmploye objectAtIndex:0];
        
    }else{
        employe = [NSEntityDescription insertNewObjectForEntityForName:@"Employes" inManagedObjectContext:self.managedObjectContext];
        
    }
    
    
    [employe setId:@([employeDict[@"Employe"][@"id"] intValue])];
    [employe setNom:NULL_TO_NIL(employeDict[@"Employe"][@"nom"])];
    [employe setPrenom:NULL_TO_NIL(employeDict[@"Employe"][@"prenom"])];
    [employe setTel:NULL_TO_NIL(employeDict[@"Employe"][@"tel"])];
    [employe setVille:NULL_TO_NIL(employeDict[@"Employe"][@"ville"])];
    [employe setAdresse:NULL_TO_NIL(employeDict[@"Employe"][@"adresse"])];
    [employe setCommentaire:NULL_TO_NIL(employeDict[@"Employe"][@"commentaire"])];
    [employe setInterim:@([employeDict[@"Employe"][@"interim"] intValue])];
    
    [employe setChef:chef];
    [employe setAgence_id:nil];
    [employe setFiches:nil];
    
    
    return employe;
    
}


// CHANTIER FCTS


-(void) updateChantier:(NSArray *)chantiersArray{
    
    [self deleteAllChantiers];
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest;
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Chantiers"
                                              inManagedObjectContext:managedObjectContext];
    
    NSPredicate *predicate;
    NSError* error;
    if([chantiersArray count]){
        
        for (NSDictionary *chantierDict in chantiersArray ){
            
            fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            predicate = [NSPredicate predicateWithFormat:@"id==%d", [chantierDict[@"Chantier"][@"id"] intValue]];
            [fetchRequest setPredicate:predicate];
            
            
            // Query on managedObjectContext With Generated fetchRequest
            NSArray *fetchedChantier = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            Chantiers *chantier;
            
            if([fetchedChantier count]==1){
                
                chantier = [fetchedChantier objectAtIndex:0];
                
            }else{
                chantier = [NSEntityDescription insertNewObjectForEntityForName:@"Chantiers" inManagedObjectContext:self.managedObjectContext];
            }
            
            [chantier setAdresse:NULL_TO_NIL(chantierDict[@"Chantier"][@"adresse"])];
            [chantier setClos:@([chantierDict[@"Chantier"][@"clos"] intValue])];
            [chantier setCommentaire:NULL_TO_NIL(chantierDict[@"Chantier"][@"commentaire"])];
            [chantier setId:@([chantierDict[@"Chantier"][@"id"] intValue])];
            [chantier setNom:NULL_TO_NIL(chantierDict[@"Chantier"][@"nom"])];
            [chantier setNum:NULL_TO_NIL(chantierDict[@"Chantier"][@"num"])];
            [chantier setTel:NULL_TO_NIL(chantierDict[@"Chantier"][@"tel"])];
            [chantier setVille:NULL_TO_NIL(chantierDict[@"Chantier"][@"ville"])];
            
            Chefs *chefLog = [self getChefLog];
            
            fetchRequest = [[NSFetchRequest alloc] init];
            
            //Setting Entity to be Queried
             NSEntityDescription *entityAssignations = [NSEntityDescription entityForName:@"Assignations_chantier"
                                 inManagedObjectContext:managedObjectContext];
            [fetchRequest setEntity:entityAssignations];
            
            
            
            predicate = [NSPredicate predicateWithFormat:@"chantier.id==%d", [chantierDict[@"Chantier"][@"id"] intValue]];
            [fetchRequest setPredicate:predicate];
            
            NSArray *fetchedAssignations = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            NSMutableSet *chefMS;
            
            Assignations_chantier *assignations_chantier;
            
            if([fetchedAssignations count]==1){
                
                assignations_chantier = [fetchedAssignations objectAtIndex:0];
                
                if(![assignations_chantier.chef containsObject:chefLog]){
                    
                    for(Chefs *chef in assignations_chantier.chef){
                        
                        [chefMS addObject:chef];
                        
                    }
                    [chefMS addObject:chefLog];
                }
                
                //ELSE ON NE CHANGE RIEN
                
                
            }else{
                
                // AUcun assignations
                assignations_chantier = [NSEntityDescription insertNewObjectForEntityForName:@"Assignations_chantier" inManagedObjectContext:managedObjectContext];
                [assignations_chantier setChantier:chantier];
                
                chefMS = [[NSMutableSet alloc]init];
                
                [chefMS addObject:chefLog];
                
            }
            
            NSMutableSet *assignationsMS = [[NSMutableSet alloc]init];
            [assignationsMS addObject:assignations_chantier];
            
            /// SET assignations ///
            [chantier setAssignation:assignations_chantier];
            
            
            assignationsMS = [[NSMutableSet alloc]init];
            if(![chefLog.assignations containsObject:assignations_chantier]){
                
                for(Assignations_chantier *ass_chantier in chefLog.assignations){
                    
                    [assignationsMS addObject:ass_chantier];
                    
                }
                [assignationsMS addObject:assignations_chantier];
                
                [chefLog setAssignations:assignationsMS];
            }

           
            
            // SAVE //
            if (![managedObjectContext save:&error]) {
                
                //Handle any error with the saving of the context
            }
            
        }
        
    }
    
    
}

- (Chantiers *) getChantier:(NSString *)chantier_id{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest;
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Chantiers"
                                              inManagedObjectContext:managedObjectContext];
    
    NSPredicate *predicate;
    NSError* error;
  
            
    fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    
    NSArray *fetchedChantier = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    predicate = [NSPredicate predicateWithFormat:@"id==%d",[chantier_id intValue]];
    [fetchRequest setPredicate:predicate];
    
    
    // Query on managedObjectContext With Generated fetchRequest
    fetchedChantier = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Chantiers *chantier = [fetchedChantier objectAtIndex:0];
        

    
    return chantier;
    
}

-(NSArray *) getChantiers{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    Chefs *chefLog = [self getChefLog];
    
    NSMutableSet *chantierMS = [[NSMutableSet alloc] init];
    
    for (Assignations_chantier *assignations_chantier in chefLog.assignations) {
    
        NSDictionary *chantierDict =
        @{
          @"id" : [assignations_chantier.chantier.id stringValue],
          @"num" : assignations_chantier.chantier.num,
          @"nom" : assignations_chantier.chantier.nom,
          @"tel" : assignations_chantier.chantier.tel,
          @"ville" : assignations_chantier.chantier.ville,
          @"adresse" : assignations_chantier.chantier.adresse,
          @"commentaire": assignations_chantier.chantier.commentaire,
          
          };
        
        chantierDict =
        @{
          @"Chantier" : chantierDict,
          
          };
        
        [chantierMS addObject:chantierDict];
        
    }
    
    NSSortDescriptor* sortDescriptorNom = [[NSSortDescriptor alloc]
                                           initWithKey:@"Chantier.nom" ascending:YES];
    NSSortDescriptor* sortDescriptorNum = [[NSSortDescriptor alloc]
                                              initWithKey:@"Chantier.num" ascending:YES];
    NSArray* sortDescriptors = [[NSArray alloc] initWithObjects: sortDescriptorNom,sortDescriptorNum, nil];
    
    return [[chantierMS allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    
    
}

-(void) deleteAllChantiers{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Chantiers"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];

    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedChantiers = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //error handling goes here
    for (Chantiers * chantier in fetchedChantiers) {
        [managedObjectContext deleteObject:chantier];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    //more error handling here
    
}



// EMPLOYE NOT INTERIM



-(void) updateEmployesNotInterim:(NSArray *)employesArray{
    
    [self deleteAllEmployes];
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest;
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Employes"
                                              inManagedObjectContext:managedObjectContext];
    
    NSPredicate *predicate;
    NSError* error;
    if([employesArray count]){
        
        for (NSDictionary *employeDict in employesArray ){
            
            fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            predicate = [NSPredicate predicateWithFormat:@"id==%d", [employeDict[@"Employe"][@"id"] intValue]];
            [fetchRequest setPredicate:predicate];
            
            
            // Query on managedObjectContext With Generated fetchRequest
            NSArray *fetchedEmployes = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            Employes *employe;
            
            if([fetchedEmployes count]==1){
                
                employe = [fetchedEmployes objectAtIndex:0];
                
            }else{
                employe = [NSEntityDescription insertNewObjectForEntityForName:@"Employes" inManagedObjectContext:managedObjectContext];
            }
            
            [employe setAdresse:NULL_TO_NIL(employeDict[@"Employe"][@"adresse"])];
            [employe setCommentaire:NULL_TO_NIL(employeDict[@"Employe"][@"commentaire"])];
            [employe setId:@([employeDict[@"Employe"][@"id"] intValue])];
            [employe setInterim:@([employeDict[@"Employe"][@"interim"] intValue])];
            [employe setNom:NULL_TO_NIL(employeDict[@"Employe"][@"nom"])];
            [employe setPrenom:NULL_TO_NIL(employeDict[@"Employe"][@"prenom"])];
            [employe setTel:NULL_TO_NIL(employeDict[@"Employe"][@"tel"])];
            [employe setVille:NULL_TO_NIL(employeDict[@"Employe"][@"ville"])];

            
            // SAVE //
            if (![managedObjectContext save:&error]) {
                
                //Handle any error with the saving of the context
            }
            
        }
        
    }
    
    
}

-(NSArray *) getEmployesNotInterim{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Employes"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"interim==%d",0];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedEmployes = [managedObjectContext executeFetchRequest:fetchRequest error:&error];

    NSMutableSet *employesMS = [[NSMutableSet alloc] init];
    for (Employes *employe in fetchedEmployes) {
        
        
        NSDictionary *employeDict =
        @{
          
          @"id" : [employe.id stringValue],
          @"nom" : employe.nom,
          @"prenom" : employe.prenom,
          @"tel" : employe.tel,
          @"ville" : employe.ville,
          @"adresse" : employe.adresse,
          @"commentaire": employe.commentaire,
          @"interim": [employe.interim stringValue],
          
          };
        
        employeDict =
        @{
          @"Employe" : employeDict,
          
          };
        
        [employesMS addObject:employeDict];
        
    }
    
    NSSortDescriptor* sortDescriptorNom = [[NSSortDescriptor alloc]
                                           initWithKey:@"Employe.nom" ascending:YES];
    NSSortDescriptor* sortDescriptorPrenom = [[NSSortDescriptor alloc]
                                              initWithKey:@"Employe.prenom" ascending:YES];
    NSArray* sortDescriptors = [[NSArray alloc] initWithObjects: sortDescriptorNom,sortDescriptorPrenom, nil];
    
    return [[employesMS allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    
}

-(Employes *) getOrSetEmploye:(NSDictionary *)employeDict{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Employes"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id==%d",[employeDict[@"id"] intValue]];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedEmploye = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    Employes * employe;
    
    if([fetchedEmploye count]){
        
        employe = [fetchedEmploye objectAtIndex:0];
        
    }else{
        
        employe = [NSEntityDescription insertNewObjectForEntityForName:@"Employes" inManagedObjectContext:managedObjectContext];
    }
    
    [employe setAdresse:NULL_TO_NIL(employeDict[@"adresse"])];
    [employe setCommentaire:NULL_TO_NIL(employeDict[@"commentaire"])];
    [employe setId:@([employeDict[@"id"] intValue])];
    [employe setInterim:@([employeDict[@"interim"] intValue])];
    [employe setNom:NULL_TO_NIL(employeDict[@"nom"])];
    [employe setPrenom:NULL_TO_NIL(employeDict[@"prenom"])];
    [employe setTel:NULL_TO_NIL(employeDict[@"tel"])];
    [employe setVille:NULL_TO_NIL(employeDict[@"ville"])];
    
    
    
    
    return employe;
    
    
}

//INTERIM FCT


-(void) updateInterims:(NSArray *)employesArray{
    
    [self deleteAllInterims];
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest;
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Employes"
                                              inManagedObjectContext:managedObjectContext];
    
    NSPredicate *predicate;
    NSError* error;
    if([employesArray count]){
        
        for (NSDictionary *employeDict in employesArray ){
            
            fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            predicate = [NSPredicate predicateWithFormat:@"id==%d", [employeDict[@"Employe"][@"id"] intValue]];
            [fetchRequest setPredicate:predicate];
            
            
            // Query on managedObjectContext With Generated fetchRequest
            NSArray *fetchedEmployes = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            Employes *employe;
            
            if([fetchedEmployes count]==1){
                
                employe = [fetchedEmployes objectAtIndex:0];
                
            }else{
                employe = [NSEntityDescription insertNewObjectForEntityForName:@"Employes" inManagedObjectContext:managedObjectContext];
            }

            [employe setAdresse:NULL_TO_NIL(employeDict[@"Employe"][@"adresse"])];
            [employe setCommentaire:NULL_TO_NIL(employeDict[@"Employe"][@"commentaire"])];
            [employe setId:@([employeDict[@"Employe"][@"id"] intValue])];
            [employe setInterim:@([employeDict[@"Employe"][@"interim"] intValue])];
            [employe setNom:NULL_TO_NIL(employeDict[@"Employe"][@"nom"])];
            [employe setPrenom:NULL_TO_NIL(employeDict[@"Employe"][@"prenom"])];
            [employe setTel:NULL_TO_NIL(employeDict[@"Employe"][@"tel"])];
            [employe setVille:NULL_TO_NIL(employeDict[@"Employe"][@"ville"])];
            
            Agences *agence = [self updateAgence:employeDict];
            
            NSMutableSet *interimsMS = [[NSMutableSet alloc]init];
            if(![agence.employees containsObject:employe]){
                
                for(Employes *interim in agence.employees){
                    
                    [interimsMS addObject:interim];
                    
                }
                [interimsMS addObject:employe];
            }
            [employe setAgence_id:agence];
            
            // SAVE //
            if (![managedObjectContext save:&error]) {
                NSLog(@"error %@",error);
                //Handle any error with the saving of the context
            }
            
        }
        
    }
    
    
}

- (Agences *) updateAgence:(NSDictionary *)employeDict{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest;
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Agences"
                                              inManagedObjectContext:managedObjectContext];
    
    NSPredicate *predicate;
    NSError* error;
   
            
    fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    
    predicate = [NSPredicate predicateWithFormat:@"id==%d", [employeDict[@"Agence"][@"id"] intValue]];
    [fetchRequest setPredicate:predicate];
    
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedAgence = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    Agences *agence;
    
    if([fetchedAgence count]==1){
        
        agence = [fetchedAgence objectAtIndex:0];
        
    }else{
        agence = [NSEntityDescription insertNewObjectForEntityForName:@"Agences" inManagedObjectContext:managedObjectContext];
    }
    
    [agence setAdresse:NULL_TO_NIL(employeDict[@"Agence"][@"adresse"])];
    [agence setCommentaire:NULL_TO_NIL(employeDict[@"Agence"][@"commentaire"])];
    [agence setEmail:NULL_TO_NIL(employeDict[@"Agence"][@"email"])];
    [agence setId:@([employeDict[@"Agence"][@"id"] intValue])];
    [agence setNom:NULL_TO_NIL(employeDict[@"Agence"][@"nom"])];
    [agence setTel:NULL_TO_NIL(employeDict[@"Agence"][@"tel"])];
    [agence setVille:NULL_TO_NIL(employeDict[@"Agence"][@"ville"])];
    
    return agence;

    
}

-(NSArray *) getInterims{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Employes"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"interim==%d",1];
    [fetchRequest setPredicate:predicate];
    

    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedEmployes = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableSet *employesMS = [[NSMutableSet alloc] init];
    
    if([fetchedEmployes count]){
        for (Employes *employe in fetchedEmployes) {

            NSDictionary *employeDict =
            @{
              
              @"id" : [employe.id stringValue],
              @"nom" : employe.nom,
              @"prenom" : employe.prenom,
              @"tel" : employe.tel,
              @"ville" : employe.ville,
              @"adresse" : employe.adresse,
              @"commentaire": employe.commentaire,
              @"interim": [employe.interim stringValue],
              
              };
            NSDictionary *agenceDict =
            @{
              
              @"id" : [employe.agence_id.id stringValue],
              @"nom" : employe.agence_id.nom,
              @"tel" : employe.agence_id.tel,
              @"ville" : employe.agence_id.ville,
              @"adresse" : employe.agence_id.adresse,
              @"commentaire": employe.agence_id.commentaire,
              @"email": employe.agence_id.email,
              
              };
            
            employeDict =
            @{
              @"Employe" : employeDict,
              @"Agence" : agenceDict,
              };
            
            [employesMS addObject:employeDict];
            
        }

    }
    
    NSSortDescriptor* sortDescriptorNom = [[NSSortDescriptor alloc]
                                           initWithKey:@"Employe.nom" ascending:YES];
    NSSortDescriptor* sortDescriptorPrenom = [[NSSortDescriptor alloc]
                                              initWithKey:@"Employe.prenom" ascending:YES];
    NSArray* sortDescriptors = [[NSArray alloc] initWithObjects: sortDescriptorNom,sortDescriptorPrenom, nil];
    
    return [[employesMS allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    
}



-(void) deleteAllEmployes{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Employes"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"interim==%d",0];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedEmployes = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //error handling goes here
    for (Employes * employe in fetchedEmployes) {
       
        if(employe.chef==nil){
            [managedObjectContext deleteObject:employe];
        }
        
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    //more error handling here
    
}

-(void) deleteAllInterims{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Employes"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"interim==%d",1];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedEmployes = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //error handling goes here
    for (Employes * employe in fetchedEmployes) {
        [managedObjectContext deleteObject:employe];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    //more error handling here
    
}

-(void) deleteAllContacts{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Contacts"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedContacts = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //error handling goes here
    for (Contacts * contact in fetchedContacts) {
        [managedObjectContext deleteObject:contact];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    //more error handling here
    
}



// Contacts fct

-(void) updateContacts:(NSArray *)contactsArray{
    
    [self deleteAllContacts];
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest;
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Contacts"
                                              inManagedObjectContext:managedObjectContext];
    
    NSPredicate *predicate;
    NSError* error;
    if([contactsArray count]){
        
        for (NSDictionary *contactDict in contactsArray ){
            
            fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            predicate = [NSPredicate predicateWithFormat:@"id==%d", [contactDict[@"Contact"][@"id"] intValue]];
            [fetchRequest setPredicate:predicate];
            
            
            // Query on managedObjectContext With Generated fetchRequest
            NSArray *fetchedContacts = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            Contacts *contact;
            
            if([fetchedContacts count]==1){
                
                contact = [fetchedContacts objectAtIndex:0];
                
            }else{
               contact = [NSEntityDescription insertNewObjectForEntityForName:@"Contacts" inManagedObjectContext:managedObjectContext];
            }
            
            [contact setAdresse:NULL_TO_NIL(contactDict[@"Contact"][@"adresse"])];
            [contact setCommentaire:NULL_TO_NIL(contactDict[@"Contact"][@"commentaire"])];
            [contact setId:@([contactDict[@"Contact"][@"id"] intValue])];
            [contact setNom:NULL_TO_NIL(contactDict[@"Contact"][@"nom"])];
            [contact setPrenom:NULL_TO_NIL(contactDict[@"Contact"][@"prenom"])];
            [contact setTel:NULL_TO_NIL(contactDict[@"Contact"][@"tel"])];
            [contact setVille:NULL_TO_NIL(contactDict[@"Contact"][@"ville"])];
            [contact setStatut:NULL_TO_NIL(contactDict[@"Contact"][@"statut"])];
            [contact setEmail:NULL_TO_NIL(contactDict[@"Contact"][@"email"])];
            
            
            // SAVE //
            if (![managedObjectContext save:&error]) {
                
                //Handle any error with the saving of the context
            }
            
        }
        
    }
    
    
}

-(NSArray *) getContacts{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Contacts"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedContacts = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableSet *contactsMS = [[NSMutableSet alloc] init];
    
    for (Contacts *contact in fetchedContacts) {
        
        
        NSDictionary *contactDict =
        @{
          
          @"id" : [contact.id stringValue],
          @"nom" : contact.nom,
          @"prenom" : contact.prenom,
          @"tel" : contact.tel,
          @"ville" : contact.ville,
          @"adresse" : contact.adresse,
          @"commentaire": contact.commentaire,
          @"email" : contact.email,
          @"statut": contact.statut,
          
          };
        
        contactDict =
        @{
          
          @"Contact" : contactDict,
          
          };
        
        [contactsMS addObject:contactDict];
        
    }
    
    NSSortDescriptor* sortDescriptorNom = [[NSSortDescriptor alloc]
                                           initWithKey:@"Contact.nom" ascending:YES];
    NSSortDescriptor* sortDescriptorPrenom = [[NSSortDescriptor alloc]
                                              initWithKey:@"Contact.prenom" ascending:YES];
    NSArray* sortDescriptors = [[NSArray alloc] initWithObjects: sortDescriptorNom,sortDescriptorPrenom, nil];
    
    return [[contactsMS allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    
    
}



//FICHES FCTS
-(void) updateFiches:(NSArray *)fichesArray{
   
    
    //SUPPRESSION FICHES EXISTANTE
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LastFiches"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(chef.id==%d) AND (chantier_id==%@)",[CHEF_LOG intValue],CHANTIER_SELECTED];
    [fetchRequest setPredicate:predicate];
     NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedFiches = [managedObjectContext executeFetchRequest:fetchRequest error:&error];

    //error handling goes here
    for (LastFiches *fiche in fetchedFiches) {

        [managedObjectContext deleteObject:fiche];
        
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
    
    
    //AJOUT FICHES
    if([fichesArray count]){
        
        for (NSDictionary *ficheDict in fichesArray ){
            
            
            for (NSDictionary *ficheDictDetails in ficheDict[@"Fiche"][@"Details"]){
                
                NSDateFormatter * dateFormatter = [NSDateFormatter new];
                
                LastFiches *fiche = [NSEntityDescription insertNewObjectForEntityForName:@"LastFiches" inManagedObjectContext:managedObjectContext];
                
                
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                
                
                NSDate *startMorning = nil;
                if (ficheDictDetails[@"Fiche"][@"matin_debut"] != (id)[NSNull null]) {
                    startMorning = [dateFormatter dateFromString:ficheDictDetails[@"Fiche"][@"matin_debut"]];
                }
                
                NSDate *endMorning= nil;
                if (ficheDictDetails[@"Fiche"][@"matin_fin"] != (id)[NSNull null]) {
                    endMorning = [dateFormatter dateFromString:ficheDictDetails[@"Fiche"][@"matin_fin"]];
                }
                
                NSDate *startAfternoon = nil;
                if (ficheDictDetails[@"Fiche"][@"aprem_debut"] != (id)[NSNull null]) {
                    startAfternoon = [dateFormatter dateFromString:ficheDictDetails[@"Fiche"][@"aprem_debut"]];
                }
                
                NSDate *endAfternoon = nil;
                if (ficheDictDetails[@"Fiche"][@"aprem_fin"] != (id)[NSNull null]) {
                    endAfternoon = [dateFormatter dateFromString:ficheDictDetails[@"Fiche"][@"aprem_fin"]];
                }
                
                
                
                [fiche setAprem_debut:startAfternoon];
                [fiche setAprem_fin:endAfternoon];
                [fiche setCommentaire:NULL_TO_NIL(ficheDictDetails[@"Fiche"][@"commentaire"])];
                [fiche setGranddeplacement:@([ficheDictDetails[@"Fiche"][@"granddeplacement"] intValue])];
                [fiche setJour:ficheDictDetails[@"Fiche"][@"jour"]];
                [fiche setMatin_debut:startMorning];
                [fiche setMatin_fin:endMorning];
                [fiche setRepas:@([ficheDictDetails[@"Fiche"][@"repas"] intValue])];
                [fiche setTrajet:@([ficheDictDetails[@"Fiche"][@"trajet"] intValue])];
                
                Chefs *chefLog = [self getChefLog];
                [fiche setChef:chefLog];
                
                Chantiers *chantier = [self getChantier:ficheDictDetails[@"Fiche"][@"chantiers_has_chefs_chantiers_id"]];
                
                [fiche setChantier:chantier];
                
                [fiche setChantier_id:CHANTIER_SELECTED];
                
                Employes *employe = [self getOrSetEmploye:ficheDictDetails[@"Employe"]];
                
                [fiche setEmploye:employe];
            
         
                // SAVE //
                if (![managedObjectContext save:&error]) {
                    NSLog(@" error %@",error);
                    //Handle any error with the saving of the context
                }
                
                

                
            }
            
        }

        
    }
    
}

-(NSArray *) getFichesByChantierAndChef:(NSString *)chantier_id withChef:(NSString *)chef_id{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LastFiches"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(chef.id==%d) AND (chantier.id==%d)", [chantier_id intValue],[chef_id intValue]];
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"jour",  nil]];
    [fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObject:@"jour"]];
    [fetchRequest setResultType:NSDictionaryResultType];
   
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedFiches = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
     NSMutableSet *fichesMS = [[NSMutableSet alloc] init];
    
    if([fetchedFiches count]){
        

        
        for (NSDictionary* groupDict in fetchedFiches) {
            
            fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            predicate = [NSPredicate predicateWithFormat:@"(chef.id==%d) AND (chantier.id==%d) AND (jour==%@)", [chef_id intValue],[chantier_id intValue],groupDict[@"jour"]];
            [fetchRequest setPredicate:predicate];
            
            NSError* error;
            
            // Query on managedObjectContext With Generated fetchRequest
            NSArray *fetchedFichesDetails = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            NSMutableSet *fichesDetailsMS = [[NSMutableSet alloc] init];
            
            for(LastFiches *fiche in fetchedFichesDetails){
                NSDateFormatter * dateFormatter = [NSDateFormatter new];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                
                NSString *startMorning=@"";
                if(fiche.matin_debut!=nil){
                    startMorning  = [dateFormatter stringFromDate:fiche.matin_debut];
                }
                
                NSString *endMorning=@"";
                if(fiche.matin_fin!=nil){
                    endMorning = [dateFormatter stringFromDate:fiche.matin_fin];
                }
                
                NSString *startAfternoon=@"";
                if(fiche.aprem_debut!=nil){
                    startAfternoon = [dateFormatter stringFromDate:fiche.aprem_debut];
                }
                
                NSString *endAfternoon=@"";
                if(fiche.aprem_fin!=nil){
                    endAfternoon = [dateFormatter stringFromDate:fiche.aprem_fin];
                }
                
                
                NSDictionary *ficheDictDetails =
                @{
                  @"jour" : fiche.jour,
                  @"chantiers_has_chefs_chantiers_id" : [fiche.chantier.id stringValue],
                  @"chantiers_has_chefs_chefs_id" : [fiche.chef.id stringValue],
                  @"employe_id" : [fiche.employe.id stringValue],
                  @"matin_debut" : startMorning,
                  @"matin_fin" : endMorning,
                  @"aprem_debut": startAfternoon,
                  @"aprem_fin": endAfternoon,
                  @"trajet":[fiche.trajet stringValue],
                  @"repas":[fiche.repas stringValue],
                  @"granddeplacement":[fiche.granddeplacement stringValue],
                  };
                
                NSDictionary *employeDict =
                @{
                  
                  @"id" : [fiche.employe.id stringValue],
                  @"nom" : fiche.employe.nom,
                  @"prenom" : fiche.employe.prenom,
                  @"tel" : fiche.employe.tel,
                  @"ville" : fiche.employe.ville,
                  @"adresse" : fiche.employe.adresse,
                  @"commentaire": fiche.employe.commentaire,
                  @"interim": [fiche.employe.interim stringValue],
                  
                  };
                
                
                NSDictionary *ficheDict =
                @{
                  @"Fiche" : ficheDictDetails,
                  @"Employe" : employeDict,
                  
                  };
                
                [fichesDetailsMS addObject:ficheDict];
                
            }
            
            NSDictionary *infoFicheDict =
            @{
              @"jour" : groupDict[@"jour"],
              @"chantiers_has_chefs_chantiers_id" : chantier_id,
              @"chantiers_has_chefs_chefs_id" : chantier_id,
              @"Details" : [fichesDetailsMS allObjects],
             
              
              };
            
            NSDictionary *finalFicheDict =
            @{
              @"Fiche" : infoFicheDict,
              
              };
            
          [fichesMS addObject:finalFicheDict];
            
        }
       
        
    }
    
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc]
                                           initWithKey:@"Fiche.jour" ascending:NO];
 
    NSArray* sortDescriptors = [[NSArray alloc] initWithObjects: sortDescriptor, nil];
    
    return [[fichesMS allObjects] sortedArrayUsingDescriptors:sortDescriptors];

    
}

-(BOOL) getIfFichesByChantierChefJour:(NSString *)chantier_id withChef:(NSString *)chef_id withJour:(NSString *)jour{
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = appDelegate.managedObjectContextApi;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Fiche"
                                              inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(chantiers_has_chefs_chantiers_id==%d) AND (chantiers_has_chefs_chefs_id==%d) AND (jour==%@)",[chef_id intValue], [chantier_id intValue],jour];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedFiches = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    

    
    if([fetchedFiches count]){
        return YES;
    }else{
        return NO;
    }
}




@end
