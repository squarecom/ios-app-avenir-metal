//
//  WebService.h
//  AvenirMetal
//
//  Created by jb fuss on 06/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import <MapKit/MapKit.h>
#import "WebService.h"
@interface WebService : NSObject {
	NSOperationQueue *queue;
}

@property (nonatomic, strong) NSOperationQueue *queue;

// Retourne l'objet partagé du singletton
+ (id)sharedInstance;

//WebServices Methods
- (void) postAuthentification:(id)delegate withLogin:(NSString*)login andPassword:(NSString*)password;
- (void) getListeChantiersByChefChantier:(id)delegate withChef:(NSString*)idChef;
- (void) getLastFichesByChantierAndChef:(id)delegate withChantier:(NSString*)idChantier withChef:(NSString*)idChef;
- (void) getListeEmployes:(id)delegate;
- (void) getListeInterimaires:(id)delegate;
- (void) addFiche:(id)delegate withJsonDictionnary:(NSMutableDictionary *)dict;
- (void) getListeAdministration:(id)delegate;


@end


/**
 * Protocole de retour des web services - methodes a implementer dans les classes concernées
 *
 * @protocol WebServiceDelegate
 */
@protocol WebServiceDelegate <NSObject>

// Ici les méthodes de callback des webservices implémentées dans les différentes classes

@optional

- (void) wsPostAuthentificationCallBack:(NSDictionary*)userAuth;
- (void) wsPostGetChantiersCallBack:(NSDictionary *)chantiersDict;
- (void) wsPostGetFichesCallBack:(NSDictionary *)fichesDict;
- (void) wsPostGetFicheDetailsCallBack:(NSDictionary *)ficheDict;
- (void) wsPostGetEmployesCallBack:(NSDictionary *)employesDict;
- (void) wsPostGetInterimairesCallBack:(NSDictionary *)interimairesDict;
- (void) wsPostAddFiche:(NSDictionary *)responseAdd;
- (void) wsConnexionFailed;
- (void) wsPostGetListeAdministrationCallBack:(NSDictionary *)administrationDict;

- (void) wsPostAuthentificationConnexionFailed;
- (void) wsPostGetChantiersFailed;
- (void) wsPostGetEmployesFailed;
- (void) wsPostGetInterimsFailed;
- (void) wsPostGetContactsFailed;
- (void) wsPostGetFichesFailed;
@end