//
//  profilDetailsAdministrationViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 11/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "profilDetailsAdministrationViewController.h"

@interface profilDetailsAdministrationViewController ()
@property (strong, nonatomic) IBOutlet UITableViewCell *nomcell;
@property (strong, nonatomic) IBOutlet UITableViewCell *statutcell;
@property (strong, nonatomic) IBOutlet UITableViewCell *telcell;
@property (strong, nonatomic) IBOutlet UITableViewCell *mailcell;
@property (strong, nonatomic) IBOutlet UITableViewCell *adressecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *villecell;
@property (strong, nonatomic) IBOutlet UITableViewCell *comcell;
@property (strong, nonatomic) IBOutlet UIButton *telbutton;
@property (strong, nonatomic) NSString *tel;


@end

@implementation profilDetailsAdministrationViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSString *nomprenom = [NSString stringWithFormat:@"%@ %@",self.detailsAdministration[@"nom"],self.detailsAdministration[@"prenom"]];
    
    self.nomcell.textLabel.text= nomprenom;
    //self.telbutton.textLabel = self.detailsEmploye[@"tel"];
    NSString *protoctel = @"telprompt://";
    
    
    self.tel = [NSString stringWithFormat:@"%@%@", protoctel, self.detailsAdministration[@"tel"]];
    
    
    [self.telbutton setTitle:self.detailsAdministration[@"tel"] forState:
     (UIControlStateNormal)];
    
    
    self.adressecell.textLabel.text = self.detailsAdministration[@"adresse"];
    self.villecell.textLabel.text = self.detailsAdministration[@"ville"];
    self.comcell.textLabel.text = self.detailsAdministration[@"commentaire"];
    self.statutcell.textLabel.text = self.detailsAdministration[@"statut"];
    self.mailcell.textLabel.text = self.detailsAdministration[@"email"];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callNumber:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.tel]];

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */


@end
