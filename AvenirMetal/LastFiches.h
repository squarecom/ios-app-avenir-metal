//
//  LastFiches.h
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Chantiers, Chefs, Employes;

@interface LastFiches : NSManagedObject

@property (nonatomic, retain) NSDate * aprem_debut;
@property (nonatomic, retain) NSDate * aprem_fin;
@property (nonatomic, retain) NSString * commentaire;
@property (nonatomic, retain) NSNumber * granddeplacement;
@property (nonatomic, retain) NSString * jour;
@property (nonatomic, retain) NSDate * matin_debut;
@property (nonatomic, retain) NSDate * matin_fin;
@property (nonatomic, retain) NSNumber * repas;
@property (nonatomic, retain) NSNumber * trajet;
@property (nonatomic, retain) NSString * chantier_id;
@property (nonatomic, retain) Chantiers *chantier;
@property (nonatomic, retain) Chefs *chef;
@property (nonatomic, retain) Employes *employe;

@end
