//
//  NombreEmployeViewController.m
//  AvenirMetal
//
//  Created by jb fuss on 20/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "NombreEmployeViewController.h"
#import "ListeEmployesTableViewController.h"

@interface NombreEmployeViewController ()

@end

@implementation NombreEmployeViewController
@synthesize pickerView;
@synthesize fiches;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    // Do any additional setup after loading the view.
}

- (void)configureView{
    if(fiches){
        [pickerView selectRow:[fiches count]-1 inComponent:0 animated:YES];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
    
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return 100;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component

{
   NSString *output = [NSString stringWithFormat:@"%ld",row+1];
    return output;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *nomsegue = @"selectionemployes";
    if([segue.identifier isEqualToString:nomsegue]){
        
        //On recupere le destination view controller
         NSInteger numberSelected = [pickerView selectedRowInComponent:0]+1;
        ListeEmployesTableViewController* letvc;
        letvc = [segue destinationViewController];
        [letvc setNumberEmployesToSelect:numberSelected];
        [letvc setFiches:fiches];
               
    }
    
}


@end
