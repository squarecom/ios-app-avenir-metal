//
//  LastFiches.m
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "LastFiches.h"
#import "Chantiers.h"
#import "Chefs.h"
#import "Employes.h"


@implementation LastFiches

@dynamic aprem_debut;
@dynamic aprem_fin;
@dynamic commentaire;
@dynamic granddeplacement;
@dynamic jour;
@dynamic matin_debut;
@dynamic matin_fin;
@dynamic repas;
@dynamic trajet;
@dynamic chantier_id;
@dynamic chantier;
@dynamic chef;
@dynamic employe;

@end
