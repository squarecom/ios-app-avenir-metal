//
//  FicheTableViewCell.m
//  AvenirMetal
//
//  Created by jb fuss on 13/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "FicheTableViewCell.h"

@implementation FicheTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
