//
//  Chefs.m
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "Chefs.h"
#import "Assignations_chantier.h"
#import "Employes.h"
#import "LastFiches.h"


@implementation Chefs

@dynamic id;
@dynamic password;
@dynamic username;
@dynamic assignations;
@dynamic employe_id;
@dynamic fiches;

@end
