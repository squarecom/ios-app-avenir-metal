//
//  ListeEmployeesSelectedTableViewController.m
//  AvenirMetal
//
//  Created by jb fuss on 07/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "ListeEmployeesSelectedTableViewController.h"
#import "Data.h"
#import "ficheObject.h"
#import "AppDelegate.h"
#import "selectHoursTableViewController.h"

@interface ListeEmployeesSelectedTableViewController ()
    @property (strong) NSMutableDictionary* fiches;
    @property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end

@implementation ListeEmployeesSelectedTableViewController
@synthesize employeesSelected;
@synthesize lastFiches;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //2
    self.managedObjectContext = appDelegate.managedObjectContext;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [self.employeesSelected count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"employeCell" forIndexPath:indexPath];
    NSDictionary *employeDetail = [self.employeesSelected objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat: @"%@ %@",employeDetail[@"Employe"][@"nom"],employeDetail[@"Employe"][@"prenom"]];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *nomsegue = @"validateEmployeeSelected";
    if([segue.identifier isEqualToString:nomsegue]){
        
        self.fiches = [[NSMutableDictionary alloc] init];
        
        int i = 0;
        BOOL isAddToFiches;
        for ( NSDictionary *employeDetail in employeesSelected) {
            
            isAddToFiches = NO;
            
            if(lastFiches!=nil){
                
                for(ficheObject *fiche in lastFiches){
                    
                    if([employeDetail[@"Employe"][@"id"] intValue] == [fiche.employe_id intValue]){
                        self.fiches[[NSString stringWithFormat: @"%d", i]]=fiche;
                        isAddToFiches = YES;
                        i++;
                        
                    }
                }
                
            }
            if(!isAddToFiches){
                
                ficheObject *fiche = [[ficheObject alloc] init];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                
                NSString *today = [dateFormatter stringFromDate:[NSDate date]];
                fiche.jour = today;
                
                fiche.chantiers_has_chefs_chantiers_id = [NSNumber numberWithInteger:[CHANTIER_SELECTED integerValue]];
                fiche.chantiers_has_chefs_chefs_id = [NSNumber numberWithInteger:[CHEF_LOG integerValue]];
                fiche.employe_id = [NSNumber numberWithInteger:[employeDetail[@"Employe"][@"id"] integerValue]];
                fiche.name_employee_output = [NSString stringWithFormat: @"%@ %@",employeDetail[@"Employe"][@"nom"],employeDetail[@"Employe"][@"prenom"]];

                self.fiches[[NSString stringWithFormat: @"%d", i]]=fiche;
                i++;
                
            }
        }
        
       
        selectHoursTableViewController* shtvc;
        shtvc = [segue destinationViewController];
        [shtvc setFiches:self.fiches];
        
        
    }
}


@end
