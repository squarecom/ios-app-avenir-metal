//
//  Data.h
//  AvenirMetal
//
//  Created by jb fuss on 07/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Data : NSObject

extern NSString * const BASE_URL;
extern NSString * const AUTH_URL;
extern NSString * const CHANTIER_BY_ID_URL;
extern NSString * const CHANTIERS_BY_CHEF_URL;
extern NSString * const LAST_FICHES_BY_CHANTIER_URL;
extern NSString * const LAST_FICHES_BY_CHANTIER_AND_CHEF_URL;
extern NSString * const EMPLOYE_BY_ID_URL;
extern NSString * const EMPLOYEES_URL;
extern NSString * const INTERIMS_URL;
extern NSString * const ADMINISTRATION_URL;
extern NSString * const ADD_FICHE_URL;
extern NSString * CHANTIER_SELECTED;
extern NSDictionary * CHANTIER_SELECTED_INFO;
extern NSString * CHEF_LOG;
extern NSString * CHEF_LOG_INFO;
extern NSDate *date_notif;


@end
