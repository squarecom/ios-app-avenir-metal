//
//  Agences.h
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Employes;

@interface Agences : NSManagedObject

@property (nonatomic, retain) NSString * adresse;
@property (nonatomic, retain) NSString * commentaire;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * nom;
@property (nonatomic, retain) NSString * tel;
@property (nonatomic, retain) NSString * ville;
@property (nonatomic, retain) NSSet *employees;
@end

@interface Agences (CoreDataGeneratedAccessors)

- (void)addEmployeesObject:(Employes *)value;
- (void)removeEmployeesObject:(Employes *)value;
- (void)addEmployees:(NSSet *)values;
- (void)removeEmployees:(NSSet *)values;

@end
