//
//  Fiche.m
//  AvenirMetal
//
//  Created by jb fuss on 12/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "LastFiche.h"


@implementation LastFiche

@dynamic aprem_debut;
@dynamic aprem_fin;
@dynamic commentaire;
@dynamic granddeplacement;
@dynamic jour;
@dynamic matin_debut;
@dynamic matin_fin;
@dynamic repas;
@dynamic trajet;
@dynamic chantiers_id;
@dynamic chefs_id;
@dynamic employe_id;

@end
