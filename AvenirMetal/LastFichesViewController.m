//
//  LastFichesViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 02/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "LastFichesViewController.h"
#import "LastFichesParticipantsViewController.h"
#import "Data.h"
#import "ListeChantierViewController.h"
#import "ficheObject.h"
#import "AppDelegate.h"
#import "NombreEmployeViewController.h"
#import "coreDataRequests.h"


@interface LastFichesViewController ()
    @property (strong, nonatomic) NSArray* lastfiches;
    @property (strong, nonatomic) NSDictionary * fichejour;
    @property (strong,nonatomic) NSMutableSet *fiches;
    @property (assign,nonatomic) BOOL performGetLastFiche;
    @property (strong,nonatomic) LastFichesParticipantsViewController *lfpvc;
    @property (strong,nonatomic) NombreEmployeViewController *nevc;
    @property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
    @property (nonatomic,assign) UIActivityIndicatorView *spinner;
    @property (nonatomic, assign) BOOL getLastFiche;
    @property (nonatomic,strong) coreDataRequests *requests;

@end

@implementation LastFichesViewController
@synthesize tableView;

- (id)initWithStyle:(UITableViewStyle)style
{
    //self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.requests = [[coreDataRequests alloc]init];
    
    [self configureView];

    self.performGetLastFiche = NO;
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //2
    self.managedObjectContext = appDelegate.managedObjectContext;
    
   
   
    
}

-(void)configureView{
    
    self.chantier = CHANTIER_SELECTED_INFO;
    self.navigationItem.prompt= [NSString stringWithFormat: @"%@ %@",self.chantier[@"num"],self.chantier[@"nom"]];
    
    [self startSpinner];
    
    [[WebService sharedInstance] getLastFichesByChantierAndChef:self withChantier:self.chantier[@"id"] withChef:CHEF_LOG];
    
}

#pragma mark - WebServices Methods
- (void) wsPostGetFichesCallBack:(NSDictionary *)fichesDict {
    
    [self.spinner stopAnimating];
    self.lastfiches = (NSArray *)fichesDict;
    if([self.lastfiches count]){
        
        [self.requests updateFiches:self.lastfiches];
        
    }
   
    [self.tableView reloadData];
    
    
    
    
    
}

- (void) wsPostGetFichesFailed{
    
    [self.spinner stopAnimating];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Assurez vous d'avoir une connexion internet"
                                                        message:@"Les données peuvent ne pas être à jour"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
    
    self.lastfiches = [self.requests getFichesByChantierAndChef:self.chantier[@"id"] withChef:CHEF_LOG];
    [self.tableView reloadData];
    
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.lastfiches count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"fichecell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    self.fichejour = [self.lastfiches objectAtIndex:indexPath.row];
    NSString *jour = self.fichejour[@"Fiche"][@"jour"];//La date de la création de la fiche comme enregistrée en bdd

    NSDateFormatter * dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateJour = [dateFormatter dateFromString:jour];//On créé un objet date par rapportà la date de la fiche

    NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0]; //On récupère la langue du tel
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
    
    [dateFormatter setDateFormat:@"EEEE dd MMMM YYYY"];//On change le format pour que le jour soit afficher
    [dateFormatter setLocale:locale];//On change la langue de l'affichage par celle du tel
    NSString * dateString = [dateFormatter stringFromDate:dateJour]; //On formatte la date précédente
    
   /* NSString * output = [NSString stringWithFormat: @"Fiche du %@",dateString];
    cell.textLabel.text = output;*/
    
    cell.textLabel.text = dateString;
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //envoyer variable a un controller
    NSDictionary *ficheSelected = [self.lastfiches objectAtIndex:indexPath.row];
    [self.lfpvc setFichejour:ficheSelected];
    [self.lfpvc setChantier:self.chantier];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSString *nomsegue = @"participantsfiche";
    NSString *addFiche = @"nombreemployes";
    if([segue.identifier isEqualToString:nomsegue]){
        
        //On recupere le destination view controller
        self.lfpvc = [segue destinationViewController];
        
    }
    if([segue.identifier isEqualToString:addFiche]){
        
        if([self.lastfiches count]!=0){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Voulez vous charger le dernier modèle ?" message:@"" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui", nil];
            [alert show];
        }
        
        self.nevc = [segue destinationViewController];
        
    }
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){ //CHARGEMENT DU MODÈLE de la dernière fiche
        
        NSDictionary *ficheDict = [self.lastfiches objectAtIndex:0];
        
        ficheDict=ficheDict[@"Fiche"][@"Details"];
        
        NSArray *arrayFicheDetails = (NSArray *)ficheDict;
        NSMutableSet *fiches  = [NSMutableSet set];
        for (NSDictionary *ficheDetail in arrayFicheDetails){
            
            ficheObject *fiche = [[ficheObject alloc] init];
            
            fiche.chantiers_has_chefs_chantiers_id = [NSNumber numberWithInteger:[CHANTIER_SELECTED integerValue]];
            fiche.chantiers_has_chefs_chefs_id = [NSNumber numberWithInteger:[CHEF_LOG integerValue]];
            
            
            /* EMPLOYÉ INFO */
            fiche.employe_id = [NSNumber numberWithInteger:[ficheDetail[@"Employe"][@"id"] integerValue]];
            fiche.name_employee_output = [NSString stringWithFormat: @"%@ %@",ficheDetail[@"Employe"][@"nom"],ficheDetail[@"Employe"][@"prenom"]];
            
            /*HORAIRE BOULOT (MIS À LA DATE D'AUJOURD'HUI)*/
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            
            NSString *today = [dateFormatter stringFromDate:[NSDate date]];
            fiche.jour = today;
            
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            
            NSDate *dateStartMorning = [dateFormatter dateFromString:ficheDetail[@"Fiche"][@"matin_debut"]];
            NSDate *dateEndMorning = [dateFormatter dateFromString:ficheDetail[@"Fiche"][@"matin_fin"]];
            NSDate *dateStartAfternoon = [dateFormatter dateFromString:ficheDetail[@"Fiche"][@"aprem_debut"]];
            NSDate *dateEndAfternoon = [dateFormatter dateFromString:ficheDetail[@"Fiche"][@"aprem_fin"]];
            
            
            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
            [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
            NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
            [dateFormatter setLocale:frLocale];
            [dateFormatter setDateFormat:@"HH:mm:ss"];
            
            NSString *startMorning = [dateFormatter stringFromDate:dateStartMorning];
            NSString *endMorning = [dateFormatter stringFromDate:dateEndMorning];
            NSString *startAfternoon = [dateFormatter stringFromDate:dateStartAfternoon];
            NSString *endAfternoon = [dateFormatter stringFromDate:dateEndAfternoon];
            
            startMorning = [NSString stringWithFormat: @"%@ %@",today,startMorning];
            endMorning = [NSString stringWithFormat: @"%@ %@",today,endMorning];
            startAfternoon = [NSString stringWithFormat: @"%@ %@",today,startAfternoon];
            endAfternoon = [NSString stringWithFormat: @"%@ %@",today,endAfternoon];
            
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            fiche.matin_debut= [dateFormatter dateFromString:startMorning];
            fiche.matin_fin= [dateFormatter dateFromString:endMorning];
            fiche.aprem_debut= [dateFormatter dateFromString:startAfternoon];
            fiche.aprem_fin= [dateFormatter dateFromString:endAfternoon];
            
            /* TRAJET */
            fiche.trajet = @([ficheDetail[@"Fiche"][@"trajet"] intValue]);
            
            /* REPAS */
            fiche.repas = @([ficheDetail[@"Fiche"][@"repas"] intValue]);
            
            /*GRAND DEPLACEMENT*/
            fiche.granddeplacement = @([ficheDetail[@"Fiche"][@"granddeplacement"] intValue]);
            
            [fiches addObject:fiche];
          
        }
    
        [self.nevc setFiches:fiches];
        [self.nevc configureView];
        
        
    }
   
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
   
    NSString *addFiche = @"nombreemployes";
    if([identifier isEqualToString:addFiche]){
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *today = [dateFormatter stringFromDate:[NSDate date]];
        
        if([self.lastfiches count]!=0){
            
            NSDictionary *lastFicheDict = [self.lastfiches objectAtIndex:0];

            
            if([lastFicheDict[@"Fiche"][@"jour"] isEqualToString:today]){
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Vous avez déjà ajouté une fiche aujourd'hui"
                                                                    message:@""
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                [alertView show];
                
                return NO;
            }
            
        }
        
        
        if ([self.requests getIfFichesByChantierChefJour:CHEF_LOG withChef:CHANTIER_SELECTED withJour:today]){
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Vous avez déjà rempli une fiche aujourd'hui"
                                                                message:@""
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
            
            return NO;
            
            
        }
        
    }
    
    return YES;
}

-(void)startSpinner{
    UIActivityIndicatorView *spinnerObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = spinnerObject;
    self.spinner.center = CGPointMake(160, 240);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

@end
