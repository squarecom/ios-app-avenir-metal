//
//  LastFichesParticipantsViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 02/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "LastFichesParticipantsViewController.h"
#import "FicheTableViewCell.h"

@interface LastFichesParticipantsViewController ()

    @property (strong, nonatomic) NSArray * arrayFicheDetails;

@end

@implementation LastFichesParticipantsViewController

@synthesize fichejour;
@synthesize chantier;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *jour = fichejour[@"Fiche"][@"jour"];//La date de la création de la fiche comme enregistrée en bdd
    
    NSDateFormatter * dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateJour = [dateFormatter dateFromString:jour];//On créé un objet date par rapportà la date de la fiche
    
    NSString * deviceLanguage = [[NSLocale preferredLanguages] objectAtIndex:0]; //On récupère la langue du tel
    NSLocale * locale = [[NSLocale alloc] initWithLocaleIdentifier:deviceLanguage];
    
    [dateFormatter setDateFormat:@"EEEE dd MMMM YYYY"];//On change le format pour que le jour soit afficher
    [dateFormatter setLocale:locale];//On change la langue de l'affichage par celle du tel
    NSString * dateString = [dateFormatter stringFromDate:dateJour]; //On formatte la date précédente

    self.arrayFicheDetails = (NSArray *) fichejour[@"Fiche"][@"Details"];

    self.navigationItem.prompt = [NSString stringWithFormat: @"%@ %@ - Le %@",chantier[@"num"],chantier[@"nom"],dateString];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [self.arrayFicheDetails count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    FicheTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *ficheDetail = [self.arrayFicheDetails objectAtIndex:indexPath.row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];


   
    NSDate *dateStartMorning = nil;
    if (ficheDetail[@"Fiche"][@"matin_debut"] != (id)[NSNull null]) {
        dateStartMorning = [dateFormatter dateFromString:ficheDetail[@"Fiche"][@"matin_debut"]];
    }
    
    NSDate *dateEndMorning= nil;
    if (ficheDetail[@"Fiche"][@"matin_fin"] != (id)[NSNull null]) {
        dateEndMorning = [dateFormatter dateFromString:ficheDetail[@"Fiche"][@"matin_fin"]];
    }
    
    NSDate *dateStartAfternoon = nil;
    if (ficheDetail[@"Fiche"][@"aprem_debut"] != (id)[NSNull null]) {
        dateStartAfternoon = [dateFormatter dateFromString:ficheDetail[@"Fiche"][@"aprem_debut"]];
    }
    
    NSDate *dateEndAfternoon = nil;
    if (ficheDetail[@"Fiche"][@"aprem_fin"] != (id)[NSNull null]) {
        dateEndAfternoon = [dateFormatter dateFromString:ficheDetail[@"Fiche"][@"aprem_fin"]];
    }
    
    
    
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [dateFormatter setLocale:frLocale];
    [dateFormatter setDateFormat:@"HH:mm"];

    NSString *startMorning = [dateFormatter stringFromDate:dateStartMorning];
    NSString *endMorning = [dateFormatter stringFromDate:dateEndMorning];
    NSString *startAfternoon = [dateFormatter stringFromDate:dateStartAfternoon];
    NSString *endAfternoon = [dateFormatter stringFromDate:dateEndAfternoon];
    
    cell.nameEmployeeLabel.text = [NSString stringWithFormat: @"%@ %@",ficheDetail[@"Employe"][@"nom"],ficheDetail[@"Employe"][@"prenom"]];
    if(dateStartMorning !=nil || dateEndMorning !=nil){
        
        cell.morningHoursLabel.text = [NSString stringWithFormat: @"%@ - %@",startMorning,endMorning];
    }
    
    if(dateStartAfternoon !=nil || dateEndAfternoon !=nil){
        
       cell.afternoonHoursLabel.text = [NSString stringWithFormat: @"%@ - %@",startAfternoon,endAfternoon];
        
    }
    
    
    if([ficheDetail[@"Fiche"][@"granddeplacement"] boolValue]){
        cell.deplacementImage.hidden = NO;
    }
    else{
        cell.deplacementImage.hidden = YES;
    }
    
    if([ficheDetail[@"Fiche"][@"repas"] boolValue]){
        cell.repasImage.hidden = NO;
    }
    else{
        cell.repasImage.hidden = YES;
    }
    if([ficheDetail[@"Fiche"][@"trajet"] boolValue]){
        cell.trajet.hidden = NO;
    }
    else{
        cell.trajet.hidden = YES;
    }
    
    return cell;
}


@end
