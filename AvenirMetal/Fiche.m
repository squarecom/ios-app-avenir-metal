//
//  Fiche.m
//  AvenirMetal
//
//  Created by jb fuss on 03/07/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import "Fiche.h"


@implementation Fiche

@dynamic aprem_debut;
@dynamic aprem_fin;
@dynamic chantiers_has_chefs_chantiers_id;
@dynamic chantiers_has_chefs_chefs_id;
@dynamic commentaire;
@dynamic employe_id;
@dynamic granddeplacement;
@dynamic jour;
@dynamic matin_debut;
@dynamic matin_fin;
@dynamic name_employee_output;
@dynamic repas;
@dynamic trajet;

@end
