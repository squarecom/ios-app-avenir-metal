//
//  RepasDeplacementTableViewCell.h
//  AvenirMetal
//
//  Created by jb fuss on 07/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepasDeplacementTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameEmployeeCell;
@property (weak, nonatomic) IBOutlet UILabel *choiceCell;

@end
