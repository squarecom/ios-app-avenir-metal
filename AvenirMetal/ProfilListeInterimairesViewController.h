//
//  ProfilListeInterimairesViewController.h
//  AvenirMetal
//
//  Created by Antoine Favre on 21/05/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface ProfilListeInterimairesViewController : UITableViewController<WebServiceDelegate,UISearchBarDelegate, UISearchDisplayDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
