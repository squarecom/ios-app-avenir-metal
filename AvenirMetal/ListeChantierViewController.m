//
//  ListeChantierViewController.m
//  AvenirMetal
//
//  Created by Antoine Favre on 27/04/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//  Veuillez vous assurez d'avoir une connexion internet (Message) => Les données peuvent ne pas être à jour

#import "ListeChantierViewController.h"
#import <AFNetworking.h>
#import "LastFichesViewController.h"
#import "Data.h"
#import "Chantiers.h"
#import "coreDataRequests.h"


@interface ListeChantierViewController ()

    @property (strong,nonatomic) LastFichesViewController *lfvc;
    @property (strong) NSArray* chantiers;
    @property (strong, nonatomic) NSArray *arraychantiers;
    @property (strong, nonatomic) NSArray *searchResults;
    @property (nonatomic, assign) BOOL isSearch;
    @property (nonatomic,assign) UIActivityIndicatorView *spinner;
    @property (strong, nonatomic) NSMutableArray *mutableChantiers;
    @property (nonatomic,strong) coreDataRequests *requests;


@end

@implementation ListeChantierViewController
@synthesize searchBar;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    searchBar.delegate = (id)self;
    searchBar.showsCancelButton = YES;
    [searchBar setShowsCancelButton:YES animated:YES];
    
    self.idchef = CHEF_LOG;
    self.login = CHEF_LOG_INFO;
    
    self.requests = [[coreDataRequests alloc]init];
    
    [self startSpinner];
    
    [[WebService sharedInstance] getListeChantiersByChefChantier:(id)self withChef:self.idchef];
    
    
}

#pragma mark - WebServices Methods
- (void) wsPostGetChantiersCallBack:(NSDictionary *)chantiersDict {
    
    [self.spinner stopAnimating];
    self.chantiers = (NSArray *)chantiersDict;
    
    [self.requests updateChantier:self.chantiers];
    
    self.mutableChantiers = [NSMutableArray arrayWithArray:self.chantiers];
    [self.tableView reloadData];
    
    
}

- (void) wsPostGetChantiersFailed{
    
    [self.spinner stopAnimating];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Assurez vous d'avoir une connexion internet"
                                                        message:@"Les données peuvent ne pas être à jour"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];

    self.chantiers = [self.requests getChantiers];
    
    self.mutableChantiers = [NSMutableArray arrayWithArray:self.chantiers];
    [self.tableView reloadData];
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.isSearch) {
        return [self.searchResults count];
        
    } else {
    return [self.chantiers count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"chantiercell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *chantier;
    if (self.isSearch) {
        chantier  = [self.searchResults objectAtIndex:indexPath.row];
    } else {
       chantier  = [self.chantiers objectAtIndex:indexPath.row];
    }
  //  NSDictionary *chantier = [self.chantiers objectAtIndex:indexPath.row];
    
    NSString *numnomchantier = [NSString stringWithFormat: @"%@ %@", chantier[@"Chantier"][@"num"], chantier[@"Chantier"][@"nom"]];
    
    cell.textLabel.text = numnomchantier;
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //envoyer variable a un controller
    NSDictionary *chantier =  [self.chantiers objectAtIndex:indexPath.row];
    CHANTIER_SELECTED = chantier[@"Chantier"][@"id"];
    CHANTIER_SELECTED_INFO = chantier[@"Chantier"];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSString *nomsegue = @"choixchantiersegue";
    if([segue.identifier isEqualToString:nomsegue]){
        
        //On recupere le destination view controller
        self.lfvc = [segue destinationViewController];
        
    }
    
    
}


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        self.isSearch = FALSE;
       
    }
    else
    {
        self.isSearch = TRUE;
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"SELF.Chantier.nom contains[cd] %@ || SELF.Chantier.num contains[cd] %@  ",
                                        text,text];
        
        self.searchResults = [self.mutableChantiers filteredArrayUsingPredicate:resultPredicate];
    }
     [self.tableView reloadData];
}

-(void) searchBarSearchButtonClicked:(UISearchBar*) searchBarObject {
    //do some thing
    [self.view endEditing:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBarObject
{
    searchBar.text=@"";
    self.isSearch = FALSE;
    [self.tableView reloadData];
    [self.view endEditing:YES];
}

-(void)startSpinner{
    UIActivityIndicatorView *spinnerObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner = spinnerObject;
    self.spinner.center = CGPointMake(160, 240);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

@end
