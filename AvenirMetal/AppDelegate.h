//
//  AppDelegate.h
//  AvenirMetal
//
//  Created by jb fuss on 17/04/14.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,WebServiceDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContextApi;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator ;

-(NSArray *)getFicheWhichFailed;
-(void)addFicheWhichFailed;
-(void)deleteFicheWhichFailed:(NSArray *)fetchedFiches;
@end
