//
//  selectHoursTableViewController.h
//  AvenirMetal
//
//  Created by jb fuss on 07/06/2014.
//  Copyright (c) 2014 com.squarecom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface selectHoursTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *nameEmployeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *startMorningLabel;
@property (weak, nonatomic) IBOutlet UILabel *endMorningLabel;
@property (weak, nonatomic) IBOutlet UILabel *startAfternoonLabel;
@property (weak, nonatomic) IBOutlet UILabel *endAfternoonLabel;
@property (weak, nonatomic) NSMutableDictionary *fiches;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UITableViewCell *toolBar;

- (IBAction)cancelPressed:(id)sender;
- (IBAction)nonePressed:(id)sender;
- (IBAction)donePressed:(id)sender;

@end
